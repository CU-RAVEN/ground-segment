#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI Module 2 class (Controls)
#Implements Position_Plot w/ PyQt and ROS

import sys
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec


class module3(QWidget):
	#Initialize classwide data structures
	uav_vx = []
	uav_vy = []
	uav_vz = []	
	x_color = 'blue'
	y_color = 'red'
	z_color = 'green'
	pos_color = 'green'
	neg_color = 'red'
	link_level_limit = 45
	wifi_level_limit = -65
	
	on_style = "background-color: green; border: 2px solid grey;color: white"
	off_style = "background-color: white; border: 1px solid grey;color: black"
	neg_off_style = "background-color: #FFCCCC; border: 1px solid grey;color: black"
	neg_on_style = "background-color: red; border: 2px solid black;color: white"
	borderless_style = "background-color: white; border: 0px solid grey;color: black"

	indicator_font = QFont()
	indicator_font.setPointSize(12)
	indicator_font.setBold(False)
	
	e_font = QFont()
	e_font.setPointSize(28)
	e_font.setBold(True)
	#signal to update uav & ugv positions
	uav_add_vel = pyqtSignal(float,float,float)
	sigControlMode = pyqtSignal(Bool)
	sigPowerMode = pyqtSignal(Bool)
	sigEmergencyMode = pyqtSignal(Bool)
	sigNetworkQuality = pyqtSignal(Vector3)
	sigGPSQuality = pyqtSignal(Vector3)
	sigWiFiConnect = pyqtSignal(Bool)
	sigWiFiQuality = pyqtSignal(Vector3)
	

	def __init__(self):					#upon initialization....
		super(QWidget,self).__init__() 	#Initialize object
		self.initFig()	#Initialize matlibplot
		self.initSig()
		self.initControlLabels()
		self.initUI() 	#Function to make the widget
				
	def setColors(self,x,y,z,pos,neg):
		self.x_color = x
		self.y_color = y
		self.z_color = z
		self.pos_color = pos
		self.neg_color = neg 
	
	def initPreviousStates(self):
		self.previousControlMode = []
		self.previousPowerMode = []
		self.previousEmergencyMode = []
		self.previousGPSQuality = []

	def initSig(self):
		#connect signals to their slots (callback functions)
		self.uav_add_vel.connect(self.setUavVel)
		self.sigControlMode.connect(self.updateControlMode)
		self.sigPowerMode.connect(self.updatePowerMode)
		self.sigEmergencyMode.connect(self.updateEmergencyMode)
		self.sigNetworkQuality.connect(self.updateNetworkQuality)
		self.sigGPSQuality.connect(self.updateGPSQuality)
		self.sigWiFiConnect.connect(self.updateWiFiConnect)
		self.sigWiFiQuality.connect(self.updateWiFiQuality)
		
	def emitWiFiQuality(self,data):
		self.sigWiFiQuality.emit(data)
	
	def emitWifiConnect(self,data):
		self.sigWiFiConnect.emit(data)
	
	def emitControlMode(self,data):
		self.sigControlMode.emit(data)
		
	def emitPowerMode(self,data):
		self.sigPowerMode.emit(data)
		
	def emitEmergencyMode(self,data):
		self.sigEmergencyMode.emit(data)
		
	def emitNetworkQuality(self,data):
		self.sigNetworkQuality.emit(data)
		
	def emitGPSQuality(self,data):
		self.sigGPSQuality.emit(data)		
		
	def setUavVel(self,x,y,z):
		#when uav update signal called
		self.uav_vx.append(x)
		self.uav_vy.append(y)
		self.uav_vz.append(z)
		self.plot_uav_vel()
								
	def initFig(self):
		#declare the plot
		self.vel_fig = plt.figure()
		self.vel_canvas = FigureCanvas(self.vel_fig)#make a figure canvas
		self.vel_canvas.setParent(self)	#not necessary...?	
		self.ax1 = plt.subplot()	

		#ax2 info (altitude plot)
		self.bar_width = 0.5				#bar graph widths
		self.ax1.set_xticks([2,3,4])			#we have two bars, so 2 xticks
		self.ax1.set_xticklabels(['X-Vel', 'Y-Vel','Z-Vel'])	#x labels
		self.ax1.set_ylabel('Velocity [m/s]')
		self.ax1.set_title('Input Velocity [m/s]')
		self.ax1.axes.set_ylim([-5,5])			#axis limits
		#self.ax1.yaxis.set_label_position("right")	#move stuff to right to avoid clash with position plot
		#self.ax1.yaxis.tick_right()
		#dummy values to init plot....
		self.vel_bar = self.ax1.bar([2,3,4],[0,0,0],self.bar_width, color = [self.x_color,self.y_color,self.z_color])
		self.ax1.axhline(y=0, color='k',lw='3')
		
		self.vel_canvas.setMaximumWidth(200)
		self.vel_canvas.setMaximumHeight(200)
		
		self.vel_canvas.draw()

	def plot_uav_vel(self):
		#plot new data & colors
		self.vel_bar[0].set_height(self.uav_vx[-1])
		self.vel_bar[0].set_color(self.x_color)
		self.vel_bar[1].set_height(self.uav_vy[-1])
		self.vel_bar[1].set_color(self.y_color)
		self.vel_bar[2].set_height(self.uav_vz[-1])
		self.vel_bar[2].set_color(self.z_color)
		self.ax1.set_xticklabels(['X:{0}'.format(round(self.uav_vx[-1],2)), 'Y:{0}'.format(round(self.uav_vy[-1],2)),'Z:{0}'.format(round(self.uav_vz[-1],2))])
		#self.vel_canvas.draw()		
				
	def updateControlMode(self,data):
		#Change label styles to indicate current status
		#1 = Automatic, #0 = Manual
		#if data.data = self.previousControlMode
		if data.data:
			#set automatic mode
			self.controlmode_label3.setStyleSheet(self.on_style)
			self.controlmode_label2.setStyleSheet(self.off_style)
		else:
			#set automatic mode
			self.controlmode_label2.setStyleSheet(self.on_style)
			self.controlmode_label3.setStyleSheet(self.off_style)			
		#self.previousControlMode = data.data
		
	def updateWiFiConnect(self,data):
		#Hijack dat power mode to WiFi mode
		#Change label styles to indicate current status
		#1 = High Power #0 = Low Power
		self.powermode_label1.setText('WiFi Connect: ')
		if data.data:
			#set High Power mode
			self.powermode_label2.setStyleSheet(self.off_style)
			self.powermode_label3.setStyleSheet(self.on_style)
		else:
			#Set Low Power mode
			self.powermode_label3.setStyleSheet(self.off_style)
			self.powermode_label2.setStyleSheet(self.on_style)
	
	def updatePowerMode(self,data):
		#Change label styles to indicate current status
		#1 = High Power #0 = Low Power
		if data.data:
			#set High Power mode
			self.powermode_label2.setStyleSheet(self.off_style)
			self.powermode_label3.setStyleSheet(self.on_style)
		else:
			#Set Low Power mode
			self.powermode_label3.setStyleSheet(self.off_style)
			self.powermode_label2.setStyleSheet(self.on_style)			
		
	def updateEmergencyMode(self,data):
		#Change label styles to indicate current status
		#1 = Emergency Mode, 0 = No
		if data.data:
			self.emergencymode_label2.setStyleSheet(self.neg_on_style)
			self.emergencymode_label3.setStyleSheet(self.off_style)
		else:
			self.emergencymode_label2.setStyleSheet(self.neg_off_style)
			self.emergencymode_label3.setStyleSheet(self.on_style)
	
	def updateWiFiQuality(self,data):
		#Prints Network Quality, Changes color if necessary
		#Controls RF Quality
		if(data.x < -self.link_level_limit):
			self.control_qual.setText('Link Level:  {0}/70'.format(round(data.x,2)))
			self.control_qual.setStyleSheet(self.neg_on_style)
		else:
			self.control_qual.setText('Link Level:  {0}/70'.format(round(data.x,2)))
			self.control_qual.setStyleSheet(self.on_style)

		if(data.y < self.wifi_level_limit):
			self.network_qual.setText('WiFi Level:  {0} dBm'.format(round(data.y,2)))
			self.network_qual.setStyleSheet(self.neg_on_style)
		else:
			self.network_qual.setText('WiFi Level:  {0} dBm'.format(round(data.y,2)))
			self.network_qual.setStyleSheet(self.on_style)
	
	def updateNetworkQuality(self,data):
		#Prints Network Quality, Changes color if necessary
		#Controls RF Quality
		if(data.x < -70):
			self.control_qual.setText('Control Signal RSSI:  {0}'.format(round(data.x,2)))
			self.control_qual.setStyleSheet(self.neg_on_style)
		else:
			self.control_qual.setText('Control Signal RSSI:  {0}'.format(round(data.x,2)))
			self.control_qual.setStyleSheet(self.on_style)

		if(data.y < -70):
			self.network_qual.setText('Network Signal RSSI:  {0}'.format(round(data.y,2)))
			self.network_qual.setStyleSheet(self.neg_on_style)
		else:
			self.network_qual.setText('Network Signal RSSI:  {0}'.format(round(data.y,2)))
			self.network_qual.setStyleSheet(self.on_style)
	
	def updateGPSQuality(self,data):
		#x = HDOPs y= VDOPS, z = FIX/FLOAT
		#z=1: FIX, z = 0: FLOAT	
		#HDOPS
		self.HDOPS.setText('GPS HDOPS: '+str(round(data.x,1)))
		if (data.x < 4):
			self.HDOPS.setStyleSheet(self.neg_off_style)
		else:
			self.HDOPS.setStyleSheet(self.on_style)
		#VDOPS
		self.VDOPS.setText('GPS VDOPS: '+str(round(data.y,1)))
		if (data.y < 4):
			self.VDOPS.setStyleSheet(self.neg_off_style)
		else:
			self.VDOPS.setStyleSheet(self.on_style)
		#FIX/FLOAT
		if (data.z):
			self.fixfloat_label2.setStyleSheet(self.neg_off_style)
			self.fixfloat_label3.setStyleSheet(self.on_style)
		else:
			self.fixfloat_label2.setStyleSheet(self.neg_on_style)
			self.fixfloat_label3.setStyleSheet(self.off_style)

	def initControlLabels(self):
		
		#Make Labels
		#Control Mode
		self.controlmode_label1 = QLabel('Control Mode:')
		self.controlmode_label1.setStyleSheet(self.borderless_style)
		self.controlmode_label2 = QLabel('Manual')
		self.controlmode_label2.setStyleSheet(self.off_style)
		self.controlmode_label2.setFont(self.indicator_font)
		self.controlmode_label3 = QLabel('Automatic')
		self.controlmode_label3.setStyleSheet(self.on_style)
		self.controlmode_label3.setFont(self.indicator_font)

		#Set to control grid
		self.controlmodegrid = QHBoxLayout()
		self.controlmodegrid.addWidget(self.controlmode_label1)
		self.controlmodegrid.addWidget(self.controlmode_label2)
		self.controlmodegrid.addWidget(self.controlmode_label3)
		self.controlmodegrid.setAlignment(Qt.AlignTop)
		
		#Power Mode
		self.powermode_label1 = QLabel('Power Mode:')
		self.powermode_label1.setStyleSheet(self.borderless_style)
		self.powermode_label2 = QLabel('Low')
		self.powermode_label2.setStyleSheet(self.off_style)
		self.powermode_label2.setFont(self.indicator_font)
		self.powermode_label3 = QLabel('High')
		self.powermode_label3.setStyleSheet(self.on_style)
		self.powermode_label3.setFont(self.indicator_font)
		
		#Set to power grid
		self.powermodegrid = QHBoxLayout()
		self.powermodegrid.addWidget(self.powermode_label1)
		self.powermodegrid.addWidget(self.powermode_label2)
		self.powermodegrid.addWidget(self.powermode_label3)
		self.powermodegrid.setAlignment(Qt.AlignTop)	
		
		#Emergency Mode
		self.emergencymode_label1 = QLabel('Emergency Mode:')
		self.emergencymode_label1.setStyleSheet(self.borderless_style)
		self.emergencymode_label2 = QLabel('Active')
		self.emergencymode_label2.setStyleSheet(self.neg_off_style)
		self.emergencymode_label2.setFont(self.indicator_font)
		self.emergencymode_label3 = QLabel('Inactive')
		self.emergencymode_label3.setStyleSheet(self.on_style)
		self.emergencymode_label3.setFont(self.indicator_font)		
		
		#Set to emergency grid
		self.emergencymodegrid = QHBoxLayout()
		self.emergencymodegrid.addWidget(self.emergencymode_label1)
		self.emergencymodegrid.addWidget(self.emergencymode_label2)
		self.emergencymodegrid.addWidget(self.emergencymode_label3)	
		self.emergencymodegrid.setAlignment(Qt.AlignTop)		
		
		#Control Signal Quality
		self.control_qual = QLabel('Control Signal RSSI:________')
		self.control_qual.setStyleSheet(self.on_style)
		self.control_qual.setMaximumHeight(28)
		self.control_qual.setAlignment(Qt.AlignCenter)
		
		#Network Signal Quality
		self.network_qual = QLabel('Network Signal RSSI:________')
		self.network_qual.setStyleSheet(self.on_style)
		self.network_qual.setMaximumHeight(28)
		self.network_qual.setAlignment(Qt.AlignCenter)
		
		#GPS HDOPS
		self.HDOPS = QLabel('GPS HDOPS:________')
		self.HDOPS.setStyleSheet(self.on_style)
		self.HDOPS.setMaximumHeight(28)
		self.HDOPS.setAlignment(Qt.AlignCenter)		
		
		#GPS VDOPS
		self.VDOPS= QLabel('GPS VDOPS:________')
		self.VDOPS.setStyleSheet(self.on_style)
		self.VDOPS.setMaximumHeight(28)
		self.VDOPS.setAlignment(Qt.AlignCenter)
		
		#FIXFLOAT
		self.fixfloat_label1 = QLabel('GPS FIX/FLOAT Status:')
		self.fixfloat_label1.setStyleSheet(self.borderless_style)
		self.fixfloat_label2 = QLabel('FLOAT')
		self.fixfloat_label2.setStyleSheet(self.neg_off_style)
		self.fixfloat_label2.setFont(self.indicator_font)
		self.fixfloat_label3 = QLabel('FIX')
		self.fixfloat_label3.setStyleSheet(self.on_style)
		self.fixfloat_label3.setFont(self.indicator_font)		
		
		#Set to emergency grid
		self.fixfloatgrid = QHBoxLayout()
		self.fixfloatgrid.addWidget(self.fixfloat_label1)
		self.fixfloatgrid.addWidget(self.fixfloat_label2)
		self.fixfloatgrid.addWidget(self.fixfloat_label3)	
		self.fixfloatgrid.setAlignment(Qt.AlignTop)
		
		
		#E-Button
		self.emergency_btn = QPushButton('Stop Vehicles',self)
		self.emergency_btn.setStyleSheet("background-color: red; border: 2px outset black; border-radius:12px ;color: white")
		#self.emergrncy_btn.clicked.connect(self.e_stop)
		self.emergency_btn.setMinimumHeight(100)
		self.emergency_btn.setFont(self.e_font)
		
		#Main Grid
		self.controlpanelgrid =QVBoxLayout()
		self.controlpanelgrid.addLayout(self.controlmodegrid)
		self.controlpanelgrid.addLayout(self.powermodegrid)
		self.controlpanelgrid.addLayout(self.emergencymodegrid)
		self.controlpanelgrid.addWidget(self.control_qual)
		self.controlpanelgrid.addWidget(self.network_qual)
		self.controlpanelgrid.addWidget(self.HDOPS)
		self.controlpanelgrid.addWidget(self.VDOPS)
		self.controlpanelgrid.addLayout(self.fixfloatgrid)
		self.controlpanelgrid.addWidget(self.emergency_btn)
		self.controlpanelgrid.setAlignment(Qt.AlignLeft)
		self.controlpanelgrid.setAlignment(Qt.AlignTop)
	
	def initUI(self):	#Starts UI	
		gridmain = QGridLayout()	#Top grid layout for widgets
		gridmain.setSpacing(5)
		gridsub = QGridLayout()
		gridsub.setSpacing(5)
		
		control_title = QLabel('UAV Controls & Radio',self)
		title_font = QFont()
		title_font.setBold(True)
		title_font.setPointSize(24)
		control_title.setFont(title_font)
		control_title.setAlignment(Qt.AlignCenter)
		
		#Sizing
		control_title.setMaximumHeight(50)
		self.vel_canvas.setMaximumHeight(350)
		
		
		#ORganize Widgets in layout
		gridmain.addWidget(control_title,0,0)	#add the title
		gridsub.addWidget(self.vel_canvas,0,0)	#add the figure canvas
		gridsub.addLayout(self.controlpanelgrid,0,1)
		gridmain.addLayout(gridsub,1,0)
		
		#Make Borders
		y = QWidget()
		y.setLayout(gridmain)
		y.setStyleSheet(self.off_style)
		x = QGridLayout()
		x.addWidget(y)
		self.setLayout(x)
		#End Make Borders
		
		
		
		#Add main window layout, set properties
		#self.setLayout(gridmain)
		#self.setGeometry(0,0,700,800)
		#self.setStyleSheet("background-color: white;")
		
	def callback_uav_vel(self,data):
		#called when 'uav' is updated
		self.uav_add_vel.emit(data.x,data.y,data.z)
		#self.uav_add_vel.emit(data.linear.x,data.linear.y,data.linear.z)
		
	def callback_uav_vel_tw(self,data):
		#called when 'uav' is updated
		self.uav_add_vel.emit(data.twist.linear.x,data.twist.linear.y,data.twist.linear.z)
		#self.uav_add_vel.emit(data.linear.x,data.linear.y,data.linear.z)	
		

