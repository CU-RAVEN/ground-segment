#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI Module 2 class (Controls)
#Implements Position_Plot w/ PyQt and ROS
import cv2
from sensor_msgs.msg import Image

import sys
import rospy
import thread
from os import path
from std_msgs.msg import*
from geometry_msgs.msg import*
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec


class module4(QWidget):
	#Initialize classwide data structures
	on_style = "background-color: green; border: 2px solid grey;color: white"
	off_style = "background-color: white; border: 1px solid grey;color: black"
	neg_off_style = "background-color: #FFCCCC; border: 1px solid grey;color: black"
	neg_on_style = "background-color: red; border: 2px solid black;color: white"
	borderless_style = "background-color: white; border: 0px solid grey;color: black"

	indicator_font = QFont()
	indicator_font.setPointSize(12)
	indicator_font.setBold(False)
	
	
	
	e_font = QFont()
	e_font.setPointSize(28)
	e_font.setBold(True)
	timing_threshold = 3
	previewx = 200
	previewy = 200

	sigTrackingStatus = pyqtSignal(Bool)
	#sigPreviewImage = pyqtSignal(Image)
	sigPreviewImageCompressed = pyqtSignal(CompressedImage)
	sigPreviewImage = pyqtSignal(Image)
	sigPosEst = pyqtSignal(Vector3)
	sigTrackingTimer = pyqtSignal(Float64)

	def __init__(self):					#upon initialization....
		super(QWidget,self).__init__() 	#Initialize object
		self.initTrackingTimer()
		self.initPosEstimate()
		self.initTrackingStatus()
		self.initPreviewImage()
		self.initSig()
		self.initUI() 	#Function to make the widget 	
	
	def setPreviewSize(self,x,y):
		self.previewx = x
		self.previewy = y
		
	def initSig(self):
		#connect signals to their slots (callback functions)
		self.sigTrackingStatus.connect(self.updateTrackingStatus)
		self.sigPreviewImage.connect(self.makePreviewImg)
		self.sigPosEst.connect(self.updatePosEstimate)
		self.sigTrackingTimer.connect(self.updateTrackingTimer)
		self.sigPreviewImageCompressed.connect(self.makePreviewImgCompressed)
		
	def emitTrackingStatus(self,data):
		self.sigTrackingStatus.emit(data)

	def emitPreviewImage(self,data):
		self.sigPreviewImage.emit(data)	
		
	def emitPreviewImageCompressed(self,data):
		self.sigPreviewImageCompressed.emit(data)	

	def emitPosEst(self,data):
		self.sigPosEst.emit(data)

	def emitTrackingTimer(self,data):
		self.sigTrackingTimer.emit(data)
	
	def updatePreviewImageCompressed(self):
		#self.image = QImage.fromData(msg.data)
		try:
			self.image
		except:
			pass
		else:
			self.image_canvas.setPixmap(QPixmap(self.image).scaled(self.previewx, self.previewy))	
					
					
	def makePreviewImg(self,msg):
		self.format = QImage.Format_RGB888
		image_data = msg.data
		image_height = msg.height
		image_width = msg.width
		bytes_per_line = msg.step
		self.image = QImage(image_data,image_width,image_height,bytes_per_line,self.format)
						
	def makePreviewImgCompressed(self,msg):
		self.image = QImage.fromData(msg.data)
	
	def updatePreviewImage(self):
		#For standard
		#self.format = QImage.Format_RGB888
		#image_data = msg.data
		#image_height = msg.height
		#image_width = msg.width
		#bytes_per_line = msg.step	
		#self.image = QImage(image_data,image_width,image_height,bytes_per_line,self.format)
		self.image_canvas.setPixmap(QPixmap(self.image).scaled(self.previewx, self.previewy))
		
	def updateTrackingStatus(self,data):
		# data.data = 1, COnfirmed
		#data.data = 0, No confirmation
		if data.data:
			self.confirm_label2.setStyleSheet(self.neg_off_style)
			self.confirm_label3.setStyleSheet(self.on_style)			
		else:
			self.confirm_label2.setStyleSheet(self.neg_on_style)
			self.confirm_label3.setStyleSheet(self.off_style)
	def updatePosEstimate(self,data):
		self.posEst_xlabel.setText('X: {0} [m]'.format(round(data.x,3)))
		self.posEst_ylabel.setText('Y: {0} [m]'.format(round(data.y,3)))
		self.posEst_zlabel.setText('Z: {0} [m]'.format(round(data.z,3)))
	
	def updateTrackingTimer(self,data):
		if (data.data > 3):
			self.trackingTimer.setText('Confrim Time: {0} [sec]'.format(round(data.data,2)))
			self.trackingTimer.setStyleSheet(self.neg_on_style)
		else:
			self.trackingTimer.setText('Confrim Time: {0} [sec]'.format(round(data.data,2)))
			self.trackingTimer.setStyleSheet(self.off_style)		
	
	def initTrackingTimer(self):
		self.trackingTimer = QLabel("Confrim Time: XX [sec]",self)
		self.trackingTimer.setStyleSheet(self.off_style)
		self.trackingTimer.setAlignment(Qt.AlignCenter)
		self.trackingTimer.setMinimumWidth(200)
		self.trackingTimer.setMaximumWidth(200)
	
	def initPreviewImage(self):
		self.image_canvas = QLabel()
		#image_canvas.setPixmap(QPixmap('icons/Raven_Large.png').scaled(200, 200, Qt.KeepAspectRatio))
		self.image_canvas.setPixmap(QPixmap(path.abspath(path.dirname(__file__) + "/.." + '/icons/Raven_Large.png')).scaled(self.previewx, self.previewy))
		
	def initPosEstimate(self):
		self.posEst_label=QLabel('POS Estimate', self)
		self.posEst_label.setStyleSheet(self.borderless_style)
		self.posEst_xlabel=QLabel('X:%%[m]',self)
		self.posEst_xlabel.setStyleSheet(self.borderless_style)
		self.posEst_ylabel=QLabel('Y:%%[m]',self)
		self.posEst_ylabel.setStyleSheet(self.borderless_style)
		self.posEst_zlabel=QLabel('Z:%%[m]',self)
		self.posEst_zlabel.setStyleSheet(self.borderless_style)
		self.posVec_box = QVBoxLayout()
		self.posVec_box.addWidget(self.posEst_xlabel)
		self.posVec_box.addWidget(self.posEst_ylabel)
		self.posVec_box.addWidget(self.posEst_zlabel)
		
		self.posEst_box=QHBoxLayout()
		self.posEst_box.addWidget(self.posEst_label)
		self.posEst_box.addLayout(self.posVec_box)
		self.posEst_box.setAlignment(Qt.AlignCenter)
		
		self.posEst = QWidget()
		self.posEst.setMaximumWidth(200)
		self.posEst.setMinimumWidth(200)
		self.posEst.setLayout(self.posEst_box)
		self.posEst.setStyleSheet(self.off_style)


	def initTrackingStatus(self):
		#Tracking Status
		self.confirm_label1 = QLabel('Confirmation:',self)
		self.confirm_label2 = QLabel('Failure',self)
		self.confirm_label3 = QLabel('Success',self)
		
		#Layout
		self.confirm_labelstack = QVBoxLayout()
		self.confirm_labelstack.addWidget(self.confirm_label2)
		self.confirm_labelstack.addWidget(self.confirm_label3)
		self.confirm_chain = QHBoxLayout()
		self.confirm_chain.addWidget(self.confirm_label1)
		self.confirm_chain.addLayout(self.confirm_labelstack)
		self.confirm_chain.setAlignment(Qt.AlignCenter)
		#Styling
		self.confirm_label1.setStyleSheet(self.borderless_style)
		self.confirm_label2.setStyleSheet(self.neg_off_style)
		self.confirm_label3.setStyleSheet(self.on_style)
		
		self.tracking_status = QWidget()
		self.tracking_status.setMaximumWidth(200)
		self.tracking_status.setMinimumWidth(200)
		self.tracking_status.setLayout(self.confirm_chain)
		self.tracking_status.setStyleSheet(self.off_style)
			
	def initUI(self):	#Starts UI	
		gridmain = QGridLayout()	#Top grid layout for widgets
		gridmain.setSpacing(5)
		gridsub = QGridLayout()
		gridsub.setSpacing(5)
		
		vision_title = QLabel('Image Processing',self)
		title_font = QFont()
		title_font.setBold(True)
		title_font.setPointSize(24)
		vision_title.setFont(title_font)
		vision_title.setAlignment(Qt.AlignCenter)		
		
		infolayout = QVBoxLayout()
		infolayout.addWidget(self.tracking_status)
		infolayout.addWidget(self.posEst)
		infolayout.addWidget(self.trackingTimer)
		
		widelayout = QHBoxLayout()
		widelayout.addWidget(self.image_canvas)
		widelayout.addLayout(infolayout)
		
		gridmain.addWidget(vision_title,0,0)
		gridmain.addLayout(widelayout,1,0)
		#gridmain.addWidget(self.posEst,2,0)

		#Make Borders
		y = QWidget()
		y.setLayout(gridmain)
		y.setStyleSheet(self.off_style)
		x = QGridLayout()
		x.addWidget(y)
		self.setLayout(x)
		#End Make Borders


		#self.setLayout(gridmain)
		#self.setGeometry(0,0,700,800)
		#self.setStyleSheet("background-color: white;")
