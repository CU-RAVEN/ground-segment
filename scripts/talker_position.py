#!/usr/bin/env python
#This ROS node publishes fake uav and ugv position data
#Message type is Vector3: (float 64, float64, float64)

#This is a new comment
import rospy
from os import path
from std_msgs.msg import*# String
from geometry_msgs.msg import*#need Vector 3
from sensor_msgs.msg import Image
import random
import math
import sys
import time
import cv2
from cv_bridge import CvBridge, CvBridgeError
from scipy import misc

def random_talker():
	#initialize the ros publishers
    pub_uav = rospy.Publisher('position_uav', Vector3, queue_size=10)
    pub_ugv = rospy.Publisher('position_ugv', Vector3, queue_size=10)
    pub_temp = rospy.Publisher('uav_temp',Float64, queue_size=10)
    pub_store = rospy.Publisher('uav_storage',Vector3, queue_size=10)
    pub_bat = rospy.Publisher('uav_battery',Vector3, queue_size =10)
    pub_uav_vel = rospy.Publisher('uav_vel_input',Vector3,queue_size=10)
    pub_flight_time = rospy.Publisher('uav_flight_time',Vector3,queue_size=10)  
    pub_command_mode = rospy.Publisher('uav_command_mode',Bool,queue_size=10) 
    pub_power_mode = rospy.Publisher('uav_power_mode',Bool,queue_size=10) 
    pub_emergency_mode = rospy.Publisher('uav_emergency_mode',Bool,queue_size=10)
    pub_rf_strength = rospy.Publisher('uav_rf_strength',Vector3,queue_size=10)
    pub_gps_strength = rospy.Publisher('uav_gps_strength',Vector3,queue_size=10)
    pub_tracking_status = rospy.Publisher('uav_tracking_status',Bool,queue_size=10)
    pub_pos_est = rospy.Publisher('uav_pos_est',Vector3,queue_size=10)
    pub_tracking_timer = rospy.Publisher('uav_tracking_timer',Float64,queue_size=10)
    pub_preview_image = rospy.Publisher('uav_preview_image',Image,queue_size=5)
    
    bridge = CvBridge()
    #initialize the ROS nodes
    rospy.init_node('talker_position', anonymous=True)
    rate = rospy.Rate(20) # 1hz output rate
    uav = Vector3()
    ugv = Vector3()
    store = Vector3()
    battery = Vector3()
    flight_t = Vector3()
    rf_strength = Vector3()
    gps_strength = Vector3()
    pos_est = Vector3()
    gps_strength.z = 0
    
    command_mode = False
    emergency_mode = False
    power_mode = False
    track_status = False
    start_time = time.time()
    s_time = time.time()
    store.x = 0
    store.y = 128
    store.z = 128
    val = 0
    index = 0
    
    battery.x = 22.2 #Initial Voltage
    battery.y = 100 # percent remaining
    battery.z = 0 #mAH used???
    
    uav_vel = Vector3()
    
    
    while not rospy.is_shutdown():
		#Location Data
		val = val +.1
		index = index + 1
		uav_x = math.cos(val)
		uav_y = math.sin(val)*2*math.cos(val)
		uav_z = math.cos(val) + 3
		
		ugv_x = 0.3*math.cos(-val)
		ugv_y = 0.7*math.sin(-val)*math.sin(-val)
		ugv_z = 0.3*math.cos(val) + 1		
		
		uav.x, uav.y, uav.z = uav_x,uav_y,uav_z
		ugv.x, ugv.y, ugv.z = ugv_x,ugv_y,ugv_z
		
		#Temp Data
		temp = 80+20*math.sin(4*val)
		
		#Store Data
		store.x = store.x + random.randrange(1,15)
		if (store.x < 127):
			pass
		else:
			store.x = 0
		store.z = store.y - store.x
		
		#Battery Data
		battery.x  = battery.x - random.randrange(1,4)
		battery.y = battery.y - random.randrange(1,15)
		battery.z = battery.z + random.randrange(3,10)
		
		if (battery.x <0 or battery.y <0):
			battery.x = 22.2 #Initial Voltage
			battery.y = 100 # percent remaining
			battery.z = 0 #mAH used???
		
		#Velocity Data
		uav_vel.x = 2*math.sin(val)
		uav_vel.y = 5*math.cos(val)
		uav_vel.z = 0.1*math.sin(-val)
		
		#Time:
		current_time = time.time()
		elapsed_time = current_time - start_time
		flight_t.x = start_time
		flight_t.y = current_time
		flight_t.z = elapsed_time
		
		if (elapsed_time > 60*60):
			start_time = time.time()
		
		
		#Trackng Timer
		c_time = time.time()
		e_time = c_time - s_time
		tracking_timer = e_time
		
		if (e_time > 4.5):
			s_time = time.time()		
			
		#Commands
		if(index%3 == 0):
			if(command_mode):
				command_mode = False
			else:
				command_mode = True

		if(index%4 == 0):
			if(emergency_mode):
				emergency_mode = False
			else:
				emergency_mode = True		

		if(index%5 == 0):
			if(power_mode):
				power_mode = False
			else:
				power_mode = True
		
		#Network	
		rf_strength.x = -68 + 3.5*math.sin(val)
		rf_strength.y = -68 - 3.5*math.cos(val)
		rf_strength.z = 0
			
		#GPS
		gps_strength.x = 5.5 + 2.5*math.sin(val)	
		gps_strength.y = 5.5 + 2.5*math.cos(val)
		if(index%5 == 0):
			if(gps_strength.z):
				gps_strength.z = 0
			else:
				gps_strength.z = 1
		
		#Vision
		if(index%3 == 0):
			if(track_status):
				track_status = False
			else:
				track_status = True		
		#Tracking Timer
		
		#Pos Est
		pos_est.x = ugv.x + 0.05*random.randrange(1,4)
		pos_est.y = ugv.y + 0.05*random.randrange(1,4)
		pos_est.z = ugv.z + 0.05*random.randrange(1,4)
		
		if(index%1 == 0):
			#Preview Image
			if(index%2 == 0):
				#image_path = path.abspath("planes/plane_sides.png")
				image_path = path.abspath(path.dirname(__file__) + "/.." + "/planes/plane_sides.png")
			else:
				#image_path = path.abspath("planes/plane_fronts.png")
				image_path = path.abspath(path.dirname(__file__) + "/.." + "/planes/plane_fronts.png")
			#img = cv2.imread(image_path,0)
			img = face = misc.imread(image_path)
			image_message = bridge.cv2_to_imgmsg(img, "8UC3")
			pub_preview_image.publish(image_message)
			
		#Log info
		rospy.loginfo(uav)
		rospy.loginfo(ugv)
		rospy.loginfo(temp)
		rospy.loginfo(store)
		rospy.loginfo(battery)
		rospy.loginfo(uav_vel)
		rospy.loginfo(flight_t)
		rospy.loginfo(command_mode)
		rospy.loginfo(power_mode)
		rospy.loginfo(emergency_mode)
		rospy.loginfo(rf_strength)
		rospy.loginfo(gps_strength)
		rospy.loginfo(track_status)
		rospy.loginfo(pos_est)
		rospy.loginfo(tracking_timer)
		
		#Publish
		pub_uav.publish(uav)
		pub_ugv.publish(ugv)
		pub_temp.publish(temp)
		pub_store.publish(store)
		pub_bat.publish(battery)
		pub_uav_vel.publish(uav_vel)
		pub_flight_time.publish(flight_t)
		pub_command_mode.publish(command_mode)
		pub_power_mode.publish(power_mode)
		pub_emergency_mode.publish(emergency_mode)
		pub_rf_strength.publish(rf_strength)
		pub_gps_strength.publish(gps_strength)
		pub_tracking_status.publish(track_status)
		pub_pos_est.publish(pos_est)
		pub_tracking_timer.publish(tracking_timer)
		
		
		rate.sleep()

if __name__ == '__main__':
    try:
        random_talker()
    except rospy.ROSInterruptException:
        pass
