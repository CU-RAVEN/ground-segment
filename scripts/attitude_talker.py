#!/usr/bin/env python
#This ROS node publishes fake uav and ugv position data
#Message type is Vector3: (float 64, float64, float64)

#This is a new comment
import rospy
from std_msgs.msg import*# String
from geometry_msgs.msg import*#need Vector 3
import random
import math
import sys

def att_talker():
	#initialize the ros publishers
	pub_uav_att = rospy.Publisher('uav_attitude',Vector3,queue_size=10)
    
    #initialize the ROS nodes
	rospy.init_node('talker_attitude', anonymous=True)
	rate = rospy.Rate(40) # 1hz output rate
	attitude = Vector3()
	val = 0
    
	while not rospy.is_shutdown():
		#Location Data
		val = val +.1
		attitude.x = 10*math.sin(val)
		attitude.y = 8*math.sin(2*val)
		attitude.z = 90*math.sin(0.2*val)

		#Log info
		rospy.loginfo(attitude)
		
		#Publish
		pub_uav_att.publish(attitude)
		rate.sleep()

if __name__ == '__main__':
    try:
        att_talker()
    except rospy.ROSInterruptException:
        pass
