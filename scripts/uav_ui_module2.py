#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI module 2 class (housekeeping)
#Implements Position_Plot w/ PyQt and ROS

import sys
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec
        

class module2(QWidget):
	off_style = "background-color: white; border: 1px solid grey;color: black"
	bat_volt = []
	bat_life = []
	bat_amph = []
	bat_warn = 20
	bat_crit = 15
	store_cap = 128
	store_used = []
	store_left = []
	store_warn = 90
	store_crit = 110
	temp_pack = []
	temp_warn = 80
	temp_crit = 90
	flight_time = []
	degree_sym = u'\u2103'
	
	sigTemp = pyqtSignal(Float64) #Done
	sigTime = pyqtSignal(Vector3) #Done
	sigStore = pyqtSignal(Vector3) #Done
	sigBat = pyqtSignal(Vector3) #Done
	
	sigVolt = pyqtSignal(Float32) #Done
	
	sigPcbTemp = pyqtSignal(Float64) #Done
	sigMotor0Temp = pyqtSignal(Float64) #Done
	sigMotor1Temp = pyqtSignal(Float64) #Done
	sigMcuTemp = pyqtSignal(Float64) #Done
	
	def __init__(self):
		super(QWidget,self).__init__() 	#Initialize object	
		self.initSigs()
		self.initFigures()
		self.initUI() #Function to make the widget	
		
				
	def initSigs(self):
		self.sigTemp.connect(self.callback_temp) #Done
		self.sigTime.connect(self.callback_time) #Done
		self.sigStore.connect(self.callback_store) #Done
		self.sigBat.connect(self.callback_battery) #Done
		self.sigVolt.connect(self.callback_battery_volt) #Done
		self.sigPcbTemp.connect(self.callback_pcb) #Done
		self.sigMotor0Temp.connect(self.callback_motor0) #Done
		self.sigMotor1Temp.connect(self.callback_motor1) #Done
		self.sigMcuTemp.connect(self.callback_mcu) #Done
		
	def emitVoltage(self,data):
		self.sigVolt.emit(data)
		
	def emitPcbTemp(self,data):
		self.sigPcbTemp.emit(data)
	
	def emitTemp(self,data):
		self.sigTemp.emit(data)
		
	def emitTime(self,data):
		self.sigTime.emit(data)
		
	def emitStore(self,data):
		self.sigStore.emit(data)
		
	def emitBat(self,data):
		self.sigBat.emit(data)						
	
	def setColors(self,x,y,z):
		self.good_color = x
		self.warn_color = y
		self.crit_color = z
		
	def setTempParams(self,x,y):
		self.temp_warn = x
		self.temp_crit = y
	
	def callback_pcb(self,data):
		self.callback_temp(data)
		
	def callback_motor0(self,data):
		pass
		
	def callback_motor1(self,data):
		pass
		
	def callback_mcu(self,data):
		pass
		
	
	def callback_battery_volt(self,data):
		self.bat_volt.append(data.data)
		volt = data.data
		
		self.bat_bar.remove()
		if(volt > self.bat_warn):
			self.bat_bar = self.bat_ax.barh(1,volt,self.bar_width, color = self.good_color)
		elif (volt <= self.bat_warn and volt > self.bat_crit):
			self.bat_bar = self.bat_ax.barh(1,volt,self.bar_width, color = self.warn_color)
		else:
			self.bat_bar = self.bat_ax.barh(1,volt,self.bar_width, color = self.crit_color)
		#self.bat_canvas.draw()
		self.bat_status.setText('{0}V\n'.format(round(volt,2)))
	
	def callback_time(self,data):
		elapsed_time = data.z
		minutes = int(elapsed_time/60)
		seconds = int(elapsed_time%60)
		if(minutes < 10 and seconds <10):
			time_string = "0{0}:0{1}".format(str(minutes),str(seconds))
		elif(minutes <10):
			time_string = "0{0}:{1}".format(str(minutes),str(seconds))
		elif(seconds < 10):
			time_string = "{0}:0{1}".format(str(minutes),str(seconds))
		else:
			time_string = "{0}:{1}".format(str(minutes),str(seconds))
		self.time_disp.display(time_string)
			
	def callback_temp(self,data):
		t = data.data
		self.temp_pack.append(t)
		self.temp_bar.remove()
		if (t < self.temp_warn):
			self.temp_bar = self.temp_ax.barh(1,t,self.bar_width,color = self.good_color)
		elif (t >= self.temp_crit):
			self.temp_bar = self.temp_ax.barh(1,t,self.bar_width,color = self.crit_color)
		else:
			self.temp_bar = self.temp_ax.barh(1,t,self.bar_width,color = self.warn_color)
		#self.temp_bar[0].set_width(t)
		#self.temp_canvas.draw()
		self.temp_status.setText('{0}'.format(round(t,2))+self.degree_sym)
		
	def callback_store(self,data):
		self.store_used.append(data.x)
		self.store_cap = data.y
		self.store_left.append(data.z) #available
		store_perc = round((data.y-data.z)/(data.y)*100,2)
		used = data.y-data.z
		#Update Figure
		self.store_bar.remove()
		if (used < self.store_warn):
			self.store_bar = self.store_ax.barh(1,used,self.bar_width,color = self.good_color)
		elif (used >= self.store_crit):
			self.store_bar = self.store_ax.barh(1,used,self.bar_width,color = self.crit_color)
		else:
			self.store_bar = self.store_ax.barh(1,used,self.bar_width,color = self.warn_color)
		#self.temp_bar[0].set_width(t)
		#self.store_canvas.draw()
		self.store_status.setText('{0} GB Left \n {1}% Used'.format(data.z,store_perc))
				
	def callback_battery(self,data):
		self.bat_volt.append(data.x)
		self.bat_life.append(data.y)
		self.bat_amph.append(data.z)
		volt = data.x
		life = data.y
		
		self.bat_bar.remove()
		if(volt > self.bat_warn):
			self.bat_bar = self.bat_ax.barh(1,life,self.bar_width, color = self.good_color)
		elif (volt <= self.bat_warn and volt > self.bat_crit):
			self.bat_bar = self.bat_ax.barh(1,life,self.bar_width, color = self.warn_color)
		else:
			self.bat_bar = self.bat_ax.barh(1,life,self.bar_width, color = self.crit_color)
		#self.bat_canvas.draw()
		self.bat_status.setText('{0}V -- {1}% \n {2}mAH used'.format(data.x,data.y,data.z))
		
	def initFigures(self):
		#Temperature Figure
		self.temp_fig = plt.figure()
		self.temp_canvas = FigureCanvas(self.temp_fig)
		self.temp_canvas.setParent(self)
		
		self.temp_ax = plt.subplot()
		self.temp_ax.get_yaxis().set_visible(False)
		self.temp_ax.axes.set_xlim([50,100])
		self.temp_fig.set_size_inches(1.0,0.2)
		self.bar_width = 0.01
		self.temp_bar = self.temp_ax.barh(1,100,self.bar_width,color = 'blue')
		self.temp_canvas.draw()
		
		#Battery Figure
		self.bat_fig = plt.figure()
		self.bat_canvas = FigureCanvas(self.bat_fig)
		self.bat_canvas.setParent(self)
		
		self.bat_ax = plt.subplot()
		self.bat_ax.get_yaxis().set_visible(False)
		self.bat_ax.axes.set_xlim([0,34])
		self.bat_fig.set_size_inches(1.0,0.2)
		self.bar_width = 0.1
		self.bat_bar = self.bat_ax.barh(1,100,self.bar_width,color = 'blue')
		self.bat_canvas.draw()
		
		#Storage Figure
		self.store_fig = plt.figure()
		self.store_canvas = FigureCanvas(self.store_fig)
		self.store_canvas.setParent(self)
		
		self.store_ax = plt.subplot()
		self.store_ax.get_yaxis().set_visible(False)
		self.store_ax.axes.set_xlim([0,128])
		self.store_fig.set_size_inches(1.0,0.2)
		self.bar_width = 0.1
		self.store_bar = self.store_ax.barh(1,0,self.bar_width,color = 'blue')
		self.store_canvas.draw()
			
	def initUI(self):
		#Bat Life
		batgrid = QGridLayout()
		self.bat_label = QLabel('Battery Life',self)
		self.bat_label.setAlignment(Qt.AlignCenter)
		batgrid.addWidget(self.bat_label,0,0)
		batgrid.addWidget(self.bat_canvas,1,0)
		self.bat_status = QLabel('XX V /n YY%',self)
		self.bat_status.setAlignment(Qt.AlignCenter)
		batgrid.addWidget(self.bat_status,2,0)

		#Package Temp
		tempgrid = QGridLayout()
		self.temp_label = QLabel('Package Temperature',self)
		self.temp_label.setAlignment(Qt.AlignCenter)
		tempgrid.addWidget(self.temp_label,0,0)
		tempgrid.addWidget(self.temp_canvas,1,0)
		self.temp_status = QLabel(self.degree_sym,self)
		self.temp_status.setAlignment(Qt.AlignCenter)
		tempgrid.addWidget(self.temp_status,2,0)
		
		#Storage
		storegrid = QGridLayout()
		self.store_label = QLabel('Storage Capacity',self)
		self.store_label.setAlignment(Qt.AlignCenter)
		storegrid.addWidget(self.store_label,0,0)
		storegrid.addWidget(self.store_canvas,1,0)
		self.store_status = QLabel('XX GB \n YY%',self)
		self.store_status.setAlignment(Qt.AlignCenter)
		storegrid.addWidget(self.store_status,2,0)
		
		#Flight Time
		timegrid = QGridLayout()
		self.time_label = QLabel('Flight Time',self)
		self.time_label.setAlignment(Qt.AlignCenter)
		timegrid.addWidget(self.time_label,0,0)
		self.time_disp = QLCDNumber(self)
		self.time_disp.display("12" + ":" +"45")
		self.time_disp.setMinimumSize(200,100)
		timegrid.addWidget(self.time_disp,1,0)
		timegrid.setAlignment(Qt.AlignCenter)
		
		#Set Max Heights
		#self.time_label.setMaximumSize(150,30)
		self.store_label.setMaximumSize(150,33)
		self.temp_label.setMaximumSize(150,33)
		self.bat_label.setMaximumSize(150,33)
		
		self.store_status.setMaximumSize(150,35)
		self.temp_status.setMaximumSize(150,35)
		self.bat_status.setMaximumSize(150,35)

		self.store_status.setMinimumSize(150,30)
		self.temp_status.setMinimumSize(150,30)
		self.bat_status.setMinimumSize(150,30)

		self.store_canvas.setMaximumHeight(45)
		self.bat_canvas.setMaximumHeight(45)
		self.temp_canvas.setMaximumHeight(45)

		
		#Housekeeping Overall
		
		#Title
		house = QLabel('Housekeeping',self)
		house.setMaximumHeight(50)
		title_font = QFont()
		title_font.setBold(True)
		title_font.setPointSize(24)
		house.setFont(title_font)
		house.setAlignment(Qt.AlignCenter)
		
		#Grid
		maingrid = QGridLayout()
		maingrid.setSpacing(3)
		maingrid.addWidget(house,0,0)
		subgrid = QGridLayout()
		subgrid.addLayout(batgrid,0,0)
		subgrid.addLayout(tempgrid,0,1)
		subgrid.addLayout(storegrid,0,2)
		subgrid.addLayout(timegrid,0,3)
		maingrid.addLayout(subgrid,1,0)
		
		#Make Borders
		y = QWidget()
		y.setLayout(maingrid)
		y.setStyleSheet(self.off_style)
		x = QGridLayout()
		x.addWidget(y)
		self.setLayout(x)
		#End Make Borders
		
		#self.setLayout(maingrid)
		#self.setStyleSheet("background-color: white;")
		

