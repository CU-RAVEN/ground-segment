#!/usr/bin/env python
#UAV UI load-in
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*
from sensor_msgs.msg import*
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from rosbag import*


def initROS_listener_node(self):
	#Initialize ROS node 'listener':
	rospy.init_node('uav_ui', anonymous=True)	
	
	#Subscribe node to topic 
	#call callback(self,data) when "random" is updated
	#rospy.Subscriber('position_uav', Vector3, self.geo.callback_uav) 	#Vicon
	#rospy.Subscriber('position_ugv', Vector3, self.geo.callback_ugv) 	#Vicon
	
	
	#rospy.Subscriber('/vrpn_client_node/tars/pose',PoseStamped,self.geo.callback_ugv_v)
	#rospy.Subscriber('/vrpn_client_node/karasu/pose',PoseStamped,self.geo.callback_uav_v)
	
	rospy.Subscriber('/uav/mavros/global_position/global',NavSatFix,self.geo.callback_ugv_g) #GPS Positioning

	
	#Housekeeping
	rospy.Subscriber('/ugv_voltage_ui',Float32,self.house.emitVoltage) #Battery
	#rospy.Subscriber('/ugv_storage',Vector3,self.emit_Store) #Storage

	#Temps
	rospy.Subscriber('/pcb_temp_ui',Float64,self.house.emitPcbTemp)
	#rospy.Subscriber("/mcu_temp_ui",Float64,self.house.emit_mcu_temp)
	#rospy.Subscriber('/motor0_temp_ui',Float64,self.house.emit_motor0_temp)
	#rospy.Subscriber('/motor1_temp_ui',Float64,self.house.emit_motor1_temp)
	
	#rospy.Subscriber('uav_flight_time',Vector3,self.house.emitTime)
	
	#Commands
	#rospy.Subscriber('ugv_emergency_mode',Bool,self.controls.emitEmergencyMode)	#Watchdog
	#rospy.Subscriber('ugv_gps_strength',Vector3,self.controls.emitGPSQuality)	#Ask BlayZ
	
	rospy.Subscriber('/wifi_connected_ui', Bool,self.controls.emitWifiConnect)
	rospy.Subscriber("/ugv_network_link_level_noise_ui", Vector3,self.controls.emitWiFiQuality)
	rospy.Subscriber('/cmd_vel_ui',Vector3Stamped, self.controls.callback_uav_vel) #Commanded Velocity
	
	#Imaging	
	#rospy.Subscriber('uav_tracking_status',Bool,self.vision.emitTrackingStatus)	#NotThereYet
	#rospy.Subscriber('uav_pos_est',Vector3,self.vision.emitPosEst)				#N/A for blob detection
	#rospy.Subscriber('uav_tracking_timer',Float64,self.vision.emitTrackingTimer)	#NotThereYet
	rospy.Subscriber('/ugv/Downsampled/compressed',CompressedImage,self.vision.emitPreviewImageCompressed)	
	#rospy.Subscriber('Downsampled',Image,self.vision.emitPreviewImage)		
