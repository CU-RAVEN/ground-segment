# ui_config.py

temp_warn = 80		#warning temperatur color threshold
temp_crit  = 95		#critical temperature color threshold
bat_warn = 15 		#Battery Voltage warning threshold
bat_crit = 6	 		#Battery Critical Voltage Threshold
pos_plot_x = 6 		#inches position plot size don't mess with for now
pos_plot_y = 8	 	#inches position size does not re-scale window, no use
good_color = 'green'		 # color schemes for housekeeping
warn_color = 'yellow' 		#color schemes for housekeeping
crit_color = 'red'		#color schemes for housekeeping
pos_color = '#00CC00' 		# Green Indicator
neg_color = '#FF3333' 		# Red Indicator
x_color = 'cyan'		#Vel Bar Color
y_color = 'magenta'		#Vel Bar Color
z_color = 'green'		#Vel Bar Color
lat0 = 39.9717936
lon0 = -105.2304889
h0 = 1565

previewx = 240
previewy = 240		#Preview Image Height

list_len = 50 #Lenght of position vector stored

#Some color hex strings
#pale green #CDEB8B
#pale blue #CCE5FF
#pale red #FFCCCC
#bold red #FF3333
#bold green #00CC00
#black #000000
#white #FFFFFF
