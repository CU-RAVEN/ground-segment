#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI Module 1 class (Position)
#Implements Position_Plot w/ PyQt and ROS

import sys
import rospy
import math
from std_msgs.msg import*
from geometry_msgs.msg import*
import pygame

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec

class module1(QWidget):
	pos_plot_x = 6 #inches
	pos_plot_y = 6 #inches
	list_len = 40
	#Initialize classwide data structures
	uav_x = []
	uav_y = []
	uav_z = []	
	ugv_x = []
	ugv_y = []
	ugv_z = []
	X0 = 0
	Z0 = 0
	Y0 = 0
	h0 = 0
	lat0 = 0
	lon0 = 0
	
	off_style = "background-color: white; border: 1px solid grey;color: black"
	#signal to update uav & ugv positions
	uav_add_pos = pyqtSignal(float,float,float)#For testing
	ugv_add_pos = pyqtSignal(float,float,float)#For testing
	uav_add_pos_v = pyqtSignal(float,float,float)#For VICON
	ugv_add_pos_v = pyqtSignal(float,float,float)#For VICON
	uav_add_lat_long = pyqtSignal(float,float,float) #For GPS
	ugv_add_lat_long = pyqtSignal(float,float,float) #For GPS
	uav_add_relative = pyqtSignal(float)


	#Define the QWidget that will display "talker" message
	def __init__(self):					#upon initialization....
		super(QWidget,self).__init__() 	#Initialize object
		self.initFig()	#Initialize matlibplot
		#self.initROS_listener_node()	#Initialize ROS subscriber node
		self.initUI() #Function to make the widget
		self.initSig()
	
	def set_latlon0(self,lat,lon,h):
		a = 6378.137;
		b = 6356.75231424518;
		self.lat0 = lat
		self.lon0 = lon
		self.h0 = h
		self.Nphi0 = math.pow(a,2)/(math.pow(a*math.cos(self.lat0),2) + math.pow(math.pow(b*math.sin(self.lat0),2),2));
		self.X0 = (self.Nphi0 + self.h0)*math.cos(self.lat0)*math.cos(self.lon0);
		self.Y0 = (self.Nphi0 + self.h0)*math.cos(self.lat0)*math.sin(self.lon0);
		self.Z0 = (pow(b/a,2)*self.Nphi0 + self.h0)*math.sin(self.lat0);
		
	def initSig(self):
		#connect signals to their slots (callback functions)
		self.uav_add_pos.connect(self.setUavPos)
		self.ugv_add_pos.connect(self.setUgvPos)
		self.uav_add_pos_v.connect(self.setUavPos)
		self.ugv_add_pos_v.connect(self.setUgvPos)
		self.uav_add_lat_long.connect(self.uav_gps_handle)
		self.ugv_add_lat_long.connect(self.ugv_gps_handle)

	def uav_gps_handle(self,lat,lon,alt):
		self.convert_ENU(lat,lon,alt,1)
		
	def ugv_gps_handle(self,lat,lon,alt):
		self.convert_ENU(lat,lon,alt,0)
		
	def uav_alt_handle(self,alt):
		#self.
		pass
	
	def set_plot_size(self,x,y):
		pass #pass for now, cannot scale window....
		#self.pos_plot_x = x
		#self.pos_plot_y = y
		#self.fig.set_size_inches(self.pos_plot_x,self.pos_plot_y)
		#self.canvas.draw()
	
	def set_list_len(self,x):
		self.list_len = x
	
	def setUavPos_v(self,x,y,z):
		#when uav update signal called
		if(len(self.uav_x) < self.list_len):
			self.uav_x.append(x)
			self.uav_y.append(y)
			self.uav_z.append(z)
		else:
			self.uav_x.pop(0)
			self.uav_y.pop(0)
			self.uav_z.pop(0)
			self.uav_x.append(x)
			self.uav_y.append(y)
			self.uav_z.append(z)		
		
		self.uavxL.setText('UAV X Pos: {0} [m]'.format(round(x,3)))
		self.uavyL.setText('UAV Y Pos: {0} [m]'.format(round(y,3)))
		self.uavzL.setText('UAV Altitude: {0} [m]'.format(round(z,3)))

		#self.plot_uav()
		
	def setUgvPos_v(self,x,y,z):
		#when ugv update signal called
		if(len(self.ugv_x) < self.list_len):
			self.ugv_x.append(x)
			self.ugv_y.append(y)
			self.ugv_z.append(z)
		else:
			self.ugv_x.pop(0)
			self.ugv_y.pop(0)
			self.ugv_z.pop(0)
			self.ugv_x.append(x)
			self.ugv_y.append(y)
			self.ugv_z.append(z)
		
		self.ugvxL.setText('UGV X Pos: {0} [m]'.format(round(x,3)))
		self.ugvyL.setText('UGV Y Pos: {0} [m]'.format(round(y,3)))
		self.ugvzL.setText('UGV Altitude: {0} [m]'.format(round(z,3)))
		#self.plot_ugv()	
	
	def setUavPos(self,x,y,z):
		#when uav update signal called
		if(len(self.uav_x) < self.list_len):
			self.uav_x.append(x)
			self.uav_y.append(y)
			self.uav_z.append(z)
		else:
			self.uav_x.pop(0)
			self.uav_y.pop(0)
			self.uav_z.pop(0)
			self.uav_x.append(x)
			self.uav_y.append(y)
			self.uav_z.append(z)	
		
		self.uavxL.setText('UAV X Pos: {0} [m]'.format(round(x,3)))
		self.uavyL.setText('UAV Y Pos: {0} [m]'.format(round(y,3)))
		self.uavzL.setText('UAV Altitude: {0} [m]'.format(round(z,3)))

		#self.plot_uav()
		
	def setUgvPos(self,x,y,z):
		#when ugv update signal called
		if(len(self.ugv_x) < self.list_len):
			self.ugv_x.append(x)
			self.ugv_y.append(y)
			self.ugv_z.append(z)
		else:
			self.ugv_x.pop(0)
			self.ugv_y.pop(0)
			self.ugv_z.pop(0)
			self.ugv_x.append(x)
			self.ugv_y.append(y)
			self.ugv_z.append(z)
		
		self.ugvxL.setText('UGV X Pos: {0} [m]'.format(round(x,3)))
		self.ugvyL.setText('UGV Y Pos: {0} [m]'.format(round(y,3)))
		self.ugvzL.setText('UGV Altitude: {0} [m]'.format(round(z,3)))
		#self.plot_ugv()	
		
	def initFig(self):
		#declare the plot
		self.fig = plt.figure()
		self.canvas = FigureCanvas(self.fig)#make a figure canvas
		self.canvas.setParent(self)	#not necessary...?		
		self.gs = gridspec.GridSpec(1, 2, width_ratios=[5, 1])  #make the subplot distribution
		self.ax1 = plt.subplot(self.gs[0])	#init subplots w/5:1 distribution
		self.ax2 = plt.subplot(self.gs[1])
		self.fig.set_size_inches(self.pos_plot_x,self.pos_plot_y)
		#ax1 info (position plot)
		self.ax1.axhline(y=0,color = 'k',linewidth = 2) #cheap way to make x axis
		self.ax1.axvline(x=0,color = 'k',linewidth = 2) #cheap way to make y axis
		self.ax1.set_title('UAV and UGV Location')	#title
		
		self.ax1.set_ylabel('Y position [m]')
		self.ax1.set_xlabel('X position [m]')
		#Dummy values to plot before reveiving anything (so the remove() function doesn't break.....)
		self.uav_path = self.ax1.plot(self.uav_x,self.uav_y,':',label = "UAV Path",linewidth = 3.5,color='magenta')	
		self.ugv_path = self.ax1.plot(self.ugv_x,self.ugv_y,':',label = "UGV Path",linewidth = 3.5,color='green')	
		self.uav_loc = self.ax1.plot(self.uav_x,self.uav_y,'*',color = 'magenta',markersize = 25)
		self.ugv_loc = self.ax1.plot(self.ugv_x,self.ugv_y,'*',color="green",markersize = 25)
		self.ax1.legend(loc="upper right")	#place legend
		#self.ax1.axes.set_xlim([-3,3])		#axis limits
		#self.ax1.axes.set_ylim([-3,3])		#axis limits
		self.ax1.axis('equal')	#same ecale axes
		
		#ax2 info (altitude plot)
		self.bar_width = 0.6				#bar graph widths
		self.ax2.set_xticks([1,2])			#we have two bars, so 2 xticks
		self.ax2.set_xticklabels(['UAV', 'UGV'])	#x labels
		self.ax2.set_ylabel('Altitude [m]')
		self.ax2.set_title('Altitude')
		self.ax2.axes.set_ylim([-2,20])			#axis limits
		self.ax2.yaxis.set_label_position("right")	#move stuff to right to avoid clash with position plot
		self.ax2.yaxis.tick_right()
		#dummy values to init plot....
		self.alt_bar = self.ax2.bar([1,2],[0,0],self.bar_width, color = ['magenta','green'])

		self.canvas.draw()

	def plot_uav(self):
		if((len(self.uav_x) > 0)):		#Remove plotted lines, stars
			self.ax1.lines.remove(self.uav_loc[-1])
			self.ax1.lines.remove(self.uav_path[-1])
		self.uav_path = self.ax1.plot(self.uav_x,self.uav_y,':',label = "UAV Path",linewidth = 3.5,color='magenta')
		self.uav_loc = self.ax1.plot(self.uav_x[-1],self.uav_y[-1],'*',color = 'magenta',markersize = 25)
		self.alt_bar[0].set_height(self.uav_z[-1])
		#self.canvas.draw()		
		
	def plot_ugv(self):
		if((len(self.ugv_x) > 0) and (len(self.ugv_y) > 0)):
			self.ax1.lines.remove(self.ugv_loc[-1])	
			self.ax1.lines.remove(self.ugv_path[-1])
		self.ugv_path = self.ax1.plot(self.ugv_x,self.ugv_y,':',label = "UGV Path",linewidth = 3.5,color='green')
		self.ugv_loc = self.ax1.plot(self.ugv_x[-1],self.ugv_y[-1],'*',color="green",markersize = 25)
		self.alt_bar[1].set_height(self.ugv_z[-1])
		#self.canvas.draw()
			
	def initUI(self):	#Starts UI	
		gridmain = QGridLayout()	#Top grid layout for widgets
		gridmain.setSpacing(5)
		gridsub = QGridLayout()
		gridsub.setSpacing(5)
		
		geo = QLabel('Vehicle Positions',self)
		title_font = QFont()
		title_font.setBold(True)
		title_font.setPointSize(24)
		geo.setFont(title_font)
		geo.setAlignment(Qt.AlignCenter)
		
		#ORganize Widgets in layout
		gridmain.addWidget(geo,0,0)	#add the title
		gridmain.addWidget(self.canvas,1,0)	#add the figure canvas
		
		self.uavxL = QLabel('UAV X Pos: [m]',self)
		self.uavyL = QLabel('UAV Y Pos: [m]',self)
		self.uavzL = QLabel('UAV Altitude: [m]',self)
		
		self.ugvxL = QLabel('UGV X Pos: [m]',self)
		self.ugvyL = QLabel('UGV Y Pos: [m]',self)
		self.ugvzL = QLabel('UGV Altitude: [m]',self)
		
		gridsub.addWidget(self.uavxL,0,0)
		gridsub.addWidget(self.uavyL,0,1)
		gridsub.addWidget(self.uavzL,0,2)
		
		gridsub.addWidget(self.ugvxL,1,0)
		gridsub.addWidget(self.ugvyL,1,1)
		gridsub.addWidget(self.ugvzL,1,2)
		gridsub.setAlignment(Qt.AlignCenter)
		
		gridmain.addLayout(gridsub,2,0)
		#Make Borders
		pos_wid = QWidget()
		pos_wid.setLayout(gridmain)
		pos_wid.setStyleSheet(self.off_style)
		x = QGridLayout()
		x.addWidget(pos_wid)
		self.setLayout(x)
		#End Make Borders
		
		#Add main window layout, set properties
		#self.setLayout(gridmain)
		self.setGeometry(0,0,700,800)
		#self.setStyleSheet("background-color: white;")
		#self.setStyleSheet(self.off_style)
		
	def callback_uav(self,data):
		#called when 'uav' is updated
		self.uav_add_pos.emit(data.x,data.y,data.z)
		
	def callback_ugv(self,data):
		self.ugv_add_pos.emit(data.x,data.y,data.z)	
		
		
	def callback_uav_v(self,data):
		#called when 'uav' is updated
		self.uav_add_pos_v.emit(data.pose.position.x,data.pose.position.y,data.pose.position.z)

		
	def callback_ugv_v(self,data):
		self.ugv_add_pos_v.emit(data.pose.position.x,data.pose.position.y,data.pose.position.z)	
		
	def callback_uav_g(self,data):
		self.uav_add_lat_long.emit(data.latitude,data.longitude,data.altitude)
	
	def callback_ugv_g(self,data):
		self.ugv_add_lat_long.emit(data.latitude,data.longitude,data.altitude)
		
	def callback_relative(self,data):
		self.uav_add_relative.emit(data.x,data.y,data.z)
	
	def convert_ENU(self,lat,lon,h,d):
		#Convert Lattitude and longitude into ENU coordinates
		#Inputs: lat0,lon0,h0,lat,lon,h
		#lat0,lon0,h0 are original ugv locations
		#lat,lon,h are ugv locations throughout time

		#Semi-major and Semi-minor axis
		a = 6378.137;
		b = 6356.75231424518;
		#Geodetic to ECEF for Inital UGV pos
		#Nphi0 = math.pow(a,2)/(math.pow(a*math.cos(lat0),2) + math.pow(math.pow(b*math.sin(lat0),2),2));
		#X0 = (Nphi0 + h0)*math.cos(lat0)*math.cos(lon0);
		#Y0 = (Nphi0 + h0)*math.cos(lat0)*math.sin(lon0);
		#Z0 = (pow(b/a,2)*Nphi0 + h0)*math.sin(lat0);

		#Geodetic to ECEF for Current UGV pos
		Nphi = math.pow(a,2)/(math.pow(a*math.cos(lat),2) + math.pow(b*math.sin(lat),2));
		X = (Nphi + h)*math.cos(lat)*math.cos(lon);
		Y = (Nphi + h)*math.cos(lat)*math.sin(lon);
		Z = (math.pow(b/a,2)*Nphi + self.h0)*math.sin(lat);

		#Relative ECEF vec
		u = X-self.X0;
		v = Y-self.Y0;
		w = Z-self.Z0;

		#Cosine and Sine functions, assumes RAD!
		cosPhi = math.cos(self.lat0);
		sinPhi = math.sin(self.lat0);
		cosLambda = math.cos(self.lon0);
		sinLambda = math.sin(self.lon0);

		#ECEF to Relative ENU
		t     =  cosLambda*u + sinLambda*v;
		uEast = -sinLambda*u + cosLambda*v;
		vNorth = -sinPhi*t + cosPhi*w;
		wUp    =  cosPhi*t + sinPhi*w;
		if(d==1):
			self.setUavPos_v(uEast,vNorth,wUp)
		else:
			self.setUgvPos_v(uEast,vNorth,wUp)
		
		#Outputs uEast, vNorth, wUp relative position of ugv relative to inital ugv
