#!/usr/bin/env python
#UAV UI load-in
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*
from sensor_msgs.msg import*
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from rosbag import*


def initROS_listener_node(self):
	#Initialize ROS node 'listener':
	rospy.init_node('uav_ui', anonymous=True)	

	#rospy.Subscriber('/vrpn_client_node/tars/pose',PoseStamped,self.geo.callback_ugv_v)#VICON Positioining
	#rospy.Subscriber('/vrpn_client_node/karasu/pose',PoseStamped,self.geo.callback_uav_v)#Vicion Positioning
	
	
	rospy.Subscriber('/uav/mavros/global_position/global',NavSatFix,self.geo.callback_uav_g) #GPS Positioning
	rospy.Subscriber('/ugv/navsat/fix',NavSatFix,self.geo.callback_ugv_g) #GPS Positioning
	
	#Housekeeping
	rospy.Subscriber('/uav_battvoltage_ui',Float32,self.house.emitVoltage) #Battery
	rospy.Subscriber('/uav_storage_info',Vector3,self.house.emitStore) #Storage

	#Temps
	rospy.Subscriber('/uav_temperature_ui',Float64,self.house.emitPcbTemp)
	
	#rospy.Subscriber('uav_flight_time',Vector3,self.house.emitTime)
	
	#Commands
	#rospy.Subscriber('ugv_emergency_mode',Bool,self.controls.emitEmergencyMode)	#Watchdog
	#rospy.Subscriber('ugv_gps_strength',Vector3,self.controls.emitGPSQuality)	#Ask BlayZ
	rospy.Subscriber("/uav_network_link_level_noise_ui", Vector3,self.controls.emitWiFiQuality)
	rospy.Subscriber('/uav_commandvel_ui',TwistStamped, self.controls.callback_uav_vel_tw) #Commanded Velocity
	
	#Imaging	
	#rospy.Subscriber('uav_tracking_status',Bool,self.vision.emitTrackingStatus)	#NotThereYet
	#rospy.Subscriber('uav_pos_est',Vector3,self.vision.emitPosEst)				#N/A for blob detection
	#rospy.Subscriber('uav_tracking_timer',Float64,self.vision.emitTrackingTimer)	#NotThereYet
	rospy.Subscriber('/uav/Downsampled/compressed',CompressedImage,self.vision.emitPreviewImageCompressed)
	#rospy.Subscriber('/uav/camera/image_raw/compressed',CompressedImage,self.vision.emitPreviewImageCompressed)	
	#rospy.Subscriber('/uav/camera/image_raw',Image,self.vision.emitPreviewImage)
