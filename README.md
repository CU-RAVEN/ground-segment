# Project RAVEN User Interface

This code is for the University of Colorado 2017-18 Aerospace Engineering Sciences Senior Projects RAVEN Team. It provides a user interface for the monitoring of an unamanned ariel vehicle (UGV) and unmaneed ariel vehicle (UAV to be run on a Linux PC. It also provides tools for recording data via ROS Bags (TODO) and limited command capbility, such as an emergency stop.

## Getting Started
The main user interface is built on PyQt5 Python libraries, with graph figures from matplotlib. The attitude indicator module is built on pygame libraries, and modified code from Duncan Law (GNU General Public License 2010). The vehicle interfaceing occurs via ROS Kinetic.

### Prerequisites:
* ROS Kinetic
* Python 2.7 (No ROS Distro for Python 3)
* pygame python librarires (pip install pygame)
* matplotlib python libraries (should be installed with ROS Kinetic)
* PyQt5 python libraries (should be installed with ROS Kinetic)
* scipy python libraries (for running the image test only, not on uav_ui, should be installed with ROS Kinetic)

### Instalation
Clone the entire repository to a catkin/ROS workspace of your choice (i.e. ~/catkin_ws/src). Build the package into your workspace:
```
$ cd ~/catkin_ws
$ catkin_make
```
Update the ROS environment to the edited workspace.
```
$ .  ~/catkin_ws/devel/setup.bash
```
## Running the User Interface
The UI can be launched via a roslaunch file. There are three arguments "vehicle" is either "uav" or "ugv" (Only use ugv for now). "test" sets up a ui demo ("true" or "false"). "attitude" is either true or false
By default, attitude is true (toggles the attitude display).
By default test is false
By default vehicle is uav
The non-test code is only implemented for the ugv
It should be fairly easy to set up the position display for VICON (just be sure to make it < 7 hz). Check the ugv_config file. One vehicle UI can display position info for both vehicles.
The main UI must be closed in-window. The attitude indicator is a seperate email.
```
roslaunch ground_segment grount_segment.launch test:= false vehicle:=ugv
```
Also be sure to have a ROS master node running.
Connecting to the network:
ssh -X "user"
password
best to save this nxt part as a .sh file and source it
```
#!/bin/bash
export ROS_MASTER_URI=http://192.168.20.129:11311 #orwhatever the jackal's ip is
export ROS_IP=YOURIPRUORIPYORIPRYO
```
at this point you may need to go to your catkin ws and source setup.bash

### Conducting a test
To test the capabilities of the user interface, there are two python programs that will publish 'fake' data to the ROS topics used by the uav user interface. talker_position.py published the mojority of the data and attitude_talker.py publishes the attitude indicator data. To run these programs, use the following in the terminal, from the project directory.
```
roslaunch ground_segment grount_segment.launch test:=true vehicle:=ugv
```

## Contact
Any questions on the code may be directed to Alex Swindell: alexander.swindell@colorado.edu
Last README update: 3/22/2018
