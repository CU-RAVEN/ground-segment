#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Dial/Generic code originally by Duncan Law
#Modified for use by Alexander Swindell 

import sys
import pygame
from math import*
from pygame.locals import *
from os import path

import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import*
from sensor_msgs.msg import Image
from rosbag import*

class attitude_indicator():
	
	def __init__(self,argv):
		pygame.init()
		self.setIcon()
		self.argv = argv
		self.myfont = pygame.font.SysFont("monospace", 20)
		self.path = 'planes/'
		self.w = 300
		self.h = 660
		self.screen = pygame.display.set_mode((self.w, self.h))
		self.screen.fill(0x222222)
		self.init_dials()
		self.init_ROS_node()
		self.rate = rospy.Rate(5);
		self.loop_over()
		
		
	def setIcon(self):
		icon = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/icons/RAVEN_WHITE.png'))
		pygame.display.set_icon(icon)
		
	def loop_over(self):
		while not rospy.is_shutdown():
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					self.exit_event()
			self.rate.sleep()
	
	def init_ROS_node(self):
		rospy.init_node('uav_attitude', anonymous=True)
		#Four cases-ugv vs uav, test vs not test
		#UAV SETTINGS
		if (self.argv[1] == "uav"):
			if (self.argv[2] == 'true'): #Test is true
				rospy.Subscriber('uav_attitude',Vector3,self.update_dials_test)
			else:	#Not a test
				rospy.Subscriber('/uav/mavros/local_position/pose',PoseStamped,self.update_dials_uav)
		#UGV SETTINGS
		elif (self.argv[1] == "ugv"):
			if (self.argv[2] == 'true'): #Test is true
				rospy.Subscriber('ugv_attitude',Vector3,self.update_dials_test)
			else:	#Not a test
				rospy.Subscriber('/ugv/imu/data',Imu,self.update_dials_ugv)
				#rospy.Subscriber('ugv_attitude',Vector3,self.update_dials)
				
	def init_dials(self):
		#y_img_path = self.path + 'airplane-top-views.png'
		y_img_path = path.abspath(path.dirname(__file__) + "/.." + '/planes/HeadingIndicator_Aircraft.png')
		y_frame_path = path.abspath(path.dirname(__file__) + "/.." +  '/planes/HeadingWeel.png')
		self.yaw_ind = Generic(y_img_path,y_frame_path,0,0,0,0)	
		self.horizon_dial = Horizon(0,300,0,0)
		
	def updateText(self,data):
		myfont = pygame.font.SysFont("monospace", 20)
		degree_sym = u'\N{DEGREE SIGN}'
		self.screen.fill(0x222222,(0,600,300,660))
		r_loc = (self.w/2-60,self.h-20)
		p_loc = (self.w/2-60,self.h-35)
		y_loc = (self.w/2-60,self.h-50)	
		if (self.argv[2] == 'true'): #Test is true
			rlabel = myfont.render("Roll: "+str(round(data.x,2))+degree_sym, True, (255,255,0))
			plabel = myfont.render("Pitch: "+str(round(data.y,2))+degree_sym, True, (255,255,0))
			ylabel = myfont.render("Heading: "+str(round(data.z,2))+degree_sym, 1, (255,255,0))
		else:
			rlabel = myfont.render("Roll: "+str(round(data[0],2))+degree_sym, True, (255,255,0))
			plabel = myfont.render("Pitch: "+str(round(data[1],2))+degree_sym, True, (255,255,0))
			ylabel = myfont.render("Heading: "+str(round(data[2],2))+degree_sym, 1, (255,255,0))
		self.screen.blit(ylabel, y_loc)
		self.screen.blit(plabel, p_loc)
		self.screen.blit(rlabel, r_loc)
	
	def update_dials_test(self,data):	
		try:
			self.yaw_ind
		except:
			pass
		else:
			self.yaw_ind.update(self.screen,data.z,self.myfont)
		try:
			self.horizon_dial
		except:
			pass
		else:
			self.horizon_dial.update(self.screen,data.x,data.y,self.myfont)
			#pass
		self.updateText(data)
		pygame.display.update()
		
	def convert_to_euler(self,q):
		# roll (x-axis rotation)
		sinr = 2.0*(q.w * q.x + q.y * q.z)
		cosr = 1.0 - 2.0 * (q.x * q.x + q.y * q.y)
		roll = atan2(sinr, cosr)

		# pitch (y-axis rotation)
		sinp = 2.0*(q.w*q.y - q.z * q.x)
		if (abs(sinp) >= 1):
			pitch = copysign(M_PI / 2, sinp) # use 90 degrees if out of range
		else:
			pitch = asin(sinp)

		# yaw (z-axis rotation)
		siny = 2.0 * (q.w * q.z + q.x * q.y)
		cosy = 1.0 - 2.0 * (q.y * q.y + q.z * q.z)
		yaw = atan2(siny, cosy)
		return [-roll, -pitch, yaw]
	
	
	def update_dials_uav(self,data):
		angles = self.convert_to_euler(data.pose.orientation)
		try:
			self.yaw_ind
		except:
			pass
		else:
			offset = 33 # Degrees
			#angle = 180*acos(data.orientation.z)/pi + offset
			self.yaw_ind.update(self.screen, 60*angles[2],self.myfont)
			#self.yaw_ind.update(self.screen,data.z,self.myfont)
		try:
			self.horizon_dial
		except:
			pass
		else:
			self.horizon_dial.update(self.screen,60*angles[0],60*angles[1],self.myfont)
			#pass
		self.updateText(angles)
		pygame.display.update()
	
	def update_dials_ugv(self,data):
		angles = self.convert_to_euler(data.orientation)
		try:
			self.yaw_ind
		except:
			pass
		else:
			offset = 33 # Degrees
			#angle = 180*acos(data.orientation.z)/pi + offset
			self.yaw_ind.update(self.screen, 60*angles[2],self.myfont)
			#self.yaw_ind.update(self.screen,data.z,self.myfont)
		try:
			self.horizon_dial
		except:
			pass
		else:
			self.horizon_dial.update(self.screen,60*angles[0],60*angles[1],self.myfont)
			#pass
		self.updateText(angles)
		pygame.display.update()
		
	
	def exit_event(self):
		print "Exiting Attitude Display"
		#pygame.display.quit()
		#pygame.quit()   # end program.		
		sys.exit()

class Dial:
   """
   Generic dial type.
   """
   def __init__(self, image, frameImage, x=0, y=0, w=0, h=0):
       """
       x,y = coordinates of top left of dial.
       w,h = Width and Height of dial.
       """
       self.x = x 
       self.y = y
       self.image = image
       self.frameImage = frameImage
       self.dial = pygame.Surface(self.frameImage.get_rect()[2:4])
       self.dial.fill(0xFFFF00)
       if(w==0):
          w = self.frameImage.get_rect()[2]
       if(h==0):
          h = self.frameImage.get_rect()[3]
       self.w = w
       self.h = h
       self.pos = self.dial.get_rect()
       self.pos = self.pos.move(x, y)

   def position(self, x, y):
       """
       Reposition top,left of dial at x,y.
       """
       self.x = x 
       self.y = y
       self.pos[0] = x 
       self.pos[1] = y 

   def position_center(self, x, y):
       """
       Reposition centre of dial at x,y.
       """
       self.x = x
       self.y = y
       self.pos[0] = x - self.pos[2]/2
       self.pos[1] = y - self.pos[3]/2

   def rotate(self, image, angle):
       """
       Rotate supplied image by "angle" degrees.
       This rotates round the centre of the image. 
       If you need to offset the centre, resize the image using self.clip.
       This is used to rotate dial needles and probably doesn't need to be used externally.
       """
       tmpImage = pygame.transform.rotate(image ,angle)
       imageCentreX = tmpImage.get_rect()[0] + tmpImage.get_rect()[2]/2
       imageCentreY = tmpImage.get_rect()[1] + tmpImage.get_rect()[3]/2

       targetWidth = tmpImage.get_rect()[2]
       targetHeight = tmpImage.get_rect()[3]

       imageOut = pygame.Surface((targetWidth, targetHeight))
       imageOut.fill(0xFFFF00)
       imageOut.set_colorkey(0xFFFF00)
       imageOut.blit(tmpImage,(0,0), pygame.Rect( imageCentreX-targetWidth/2,imageCentreY-targetHeight/2, targetWidth, targetHeight ) )
       return imageOut

   def clip(self, image, x=0, y=0, w=0, h=0, oX=0, oY=0):
       """
       Cuts out a part of the needle image at x,y position to the correct size (w,h).
       This is put on to "imageOut" at an offset of oX,oY if required.
       This is used to centre dial needles and probably doesn't need to be used externally.       
       """
       if(w==0):
           w = image.get_rect()[2]
       if(h==0):
           h = image.get_rect()[3]
       needleW = w + 2*sqrt(oX*oX)
       needleH = h + 2*sqrt(oY*oY)
       imageOut = pygame.Surface((needleW, needleH))
       imageOut.fill(0xFFFF00)
       imageOut.set_colorkey(0xFFFF00)
       imageOut.blit(image, (needleW/2-w/2+oX, needleH/2-h/2+oY), pygame.Rect(x,y,w,h))
       return imageOut

   def overlay(self, image, x, y, r=0):
       """
       Overlays one image on top of another using 0xFFFF00 (Yellow) as the overlay colour.
       """
       x -= (image.get_rect()[2] - self.dial.get_rect()[2])/2
       y -= (image.get_rect()[3] - self.dial.get_rect()[3])/2
       image.set_colorkey(0xFFFF00)
       self.dial.blit(image, (x,y))


class Horizon(Dial):
	"""
	Artificial horizon dial.
	"""
	def __init__(self, x=0, y=0, w=0, h=0):
		"""
		Initialise dial at x,y.
		Default size of 300px can be overidden using w,h.
		"""
		self.image = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/planes/Horizon_GroundSky.png')).convert()
		self.frameImage = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/planes/Horizon_Background.png')).convert()
		self.maquetteImage = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/planes/Maquette_Avion.png')).convert()
		Dial.__init__(self, self.image, self.frameImage, x, y, w, h)
	def update(self, screen, angleX, angleY,myfont):
		"""
		Called to update an Artificial horizon dial.
		"angleX" and "angleY" are the inputs.
		"screen" is the surface to draw the dial on.
		"""
		angleX %= 360
		angleY %= 360
		if (angleX > 180):
			angleX -= 360 
		if (angleY > 90)and(angleY < 270):
			angleY = 180 - angleY 
		elif (angleY > 270):
			angleY -= 360
		tmpImage = self.clip(self.image, 0, (59-angleY)*720/180, 250, 250)
		tmpImage = self.rotate(tmpImage, angleX)
		self.overlay(tmpImage, 0, 0)
		self.overlay(self.frameImage, 0,0)
		self.overlay(self.maquetteImage, 0,0)
		self.dial.set_colorkey(0xFFFF00)
		screen.blit( pygame.transform.scale(self.dial,(self.w,self.h)), self.pos )
		"""
		#degree_sym = u'\N{DEGREE SIGN}'
		#p_loc = (self.w/2-60,self.h-37)
		#r_loc = (self.w/2-60,self.h-52)
		#plabel = myfont.render("Pitch: "+str(round(angleX,2))+degree_sym, 1, (255,255,0))
		#rlabel = myfont.render("Roll: "+str(round(angleY,2))+degree_sym, 1, (255,255,0))
		#screen.blit(plabel, p_loc)
		#screen.blit(rlabel, r_loc)
		"""


class Generic(Dial):
	"""
	Generic Dial. This is built on by other dials.
	"""
	def __init__(self,img_path,frame_path, x=0, y=0, w=0, h=0):
		"""
		Initialise dial at x,y.
		Default size of 300px can be overidden using w,h.       
		"""

		self.image = pygame.image.load(img_path).convert()
		#self.frameImage = pygame.image.load(frame_path).convert()
       
		#self.image = pygame.image.load('/home/alexander/Documents/Cockpit/resources/AirSpeedNeedle.png').convert()
		self.frameImage = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/planes/Indicator_Background.png')).convert()
		self.compassImage = pygame.image.load(path.abspath(path.dirname(__file__) + "/.." + '/planes/HeadingWeel.png')).convert()
		Dial.__init__(self, self.image, self.frameImage, x, y, w, h)
	def update(self, screen, angleX,myfont, iconLayer=0):
		"""
		Called to update a Generic dial.
		"angleX" and "angleY" are the inputs.
		"screen" is the surface to draw the dial on.       
		"""
		degree_sym = u'\N{DEGREE SIGN}'
		angleX %= 360
		angleX = 360 - angleX
		#tmpImage = self.clip(self.image, 0, 0, 0, 0, 0, -35)
		tmpImage = self.clip(self.image, 0, (59-0)*720/180, 250, 250)
		tmpImage = self.rotate(self.image, angleX)
		self.dial.set_colorkey(0xFFFF00)
		if iconLayer:
			self.overlay(iconLayer[0],iconLayer[1],iconLayer[2])
		self.overlay(self.frameImage, 0,0)
		self.overlay(self.compassImage,0,0)
		self.overlay(tmpImage, 0, 0)
		
		screen.blit(pygame.transform.scale(self.dial,(self.w,self.h)), self.pos )
		#font_loc = (self.w/2-60,self.h+22)
		#label = myfont.render("Yaw: "+str(round(angleX,2))+degree_sym, 1, (255,255,0))
		#screen.blit(label, font_loc)
"""		
if __name__ == '__main__':
	#initialize a QApplication
	dials = attitude_indicator(sys.argv)
	#Exit the program when app exits
	sys.exit(dials.exit_event())
"""
	
	
if __name__ == '__main__':
    try:
        module = attitude_indicator(sys.argv)
        
    except rospy.ROSInterruptException:
        module.exit_event()
	sys.exit(module.exit_event())
