#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI (Main App)
#Implements Position_Plot w/ PyQt and ROS

import sys
import rospy
import pygame
import thread


from os import path
from uav_ui_module1 import*
from uav_ui_module2 import*
from uav_ui_module3 import*
from uav_ui_module4 import*
import ui_config
import config_editor
import uav_config
import uav_test_config
import ugv_config
import ugv_test_config
#from attitude_indicator import*

from std_msgs.msg import*
from geometry_msgs.msg import*
from sensor_msgs.msg import*
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from rosbag import*

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec


class uav_ui(QMainWindow):
	off_style = "background-color: white; border: 1px solid grey;color: black"
	def __init__(self):					#upon initialization....
		super(QWidget,self).__init__() 	#Initialize object
		self.argv = sys.argv
		#Initialize modules
		self.initModules();
		#Set Settings
		self.update_settings()
		self.config_edit = config_editor.Config_Box(self)
		#Align modules
		self.makeGrid()
		#Make ROS Stuff
		self.DO_ROS()
		#self.initROS_listener_node()	#Initialize ROS subscriber node
		self.initActions()
		self.initMenu()
		self.initToolbar()
		self.initUI()					#Build Application
		self.initTimer()

	def matplot_thread(self):
		try:
			thread.start_new_thread(self.matplot_update())
		except:
			pass
			#print("Could not start matplot thread!")

	def matplot_update(self):
		self.geo.plot_uav()
		self.geo.plot_ugv()
		self.geo.canvas.draw()
		self.house.bat_canvas.draw()
		self.house.temp_canvas.draw()
		self.house.store_canvas.draw()
		self.controls.vel_canvas.draw()

	def preview_thread(self):
		try:
			thread.start_new_thread(self.vision.updatePreviewImage())
		except:
			pass	

	def preview_thread_compressed(self):
		self.vision.updatePreviewImageCompressed()

	
	def initTimer(self):
		#Timer for matplotlib updates
		self.uTime = QTimer()
		self.uTime.timeout.connect(self.matplot_thread)
		self.uTime.start(125)
		
		#Timer for preview image updates
		self.iTime = QTimer()
		if (self.argv[2] == 'true'): #Test is true
			#We are doing a full image
			self.iTime.timeout.connect(self.preview_thread)
		else:
			#We want the compressed image
			self.iTime.timeout.connect(self.preview_thread_compressed)
		self.iTime.start(100)

	def DO_ROS(self):
		#Four cases-ugv vs uav, test vs not test
		if (self.argv[1] == "uav"):
			if (self.argv[2] == 'true'): #Test is true
				uav_test_config.initROS_listener_node(self);
			else:	#Not a test
				uav_config.initROS_listener_node(self);
		elif (self.argv[1] == "ugv"):
			if (self.argv[2] == 'true'): #Test is true
				ugv_test_config.initROS_listener_node(self);
			else:	#Not a test
				ugv_config.initROS_listener_node(self);	

		else:
			print("ROS Subscription setup error!")
			print("Now Closing!")
			self.close_event();	
		
	def initModules(self):
		#Make Modules
		self.geo = module1()
		self.house = module2()
		self.controls = module3()
		self.vision = module4()
		#Set Status Tips
		self.vision.setStatusTip('UAV Imaging Data')
		self.house.setStatusTip('UAV Housekeeping Data')
		self.geo.setStatusTip('UAV and UGV Locations')
		self.controls.setStatusTip('UAV Controls')
			
	def close_event(self):
		self.close()
	
	def update_settings(self):
		reload(ui_config)
		#Temp threcholds
		try:
			ui_config.temp_warn, ui_config.temp_crit
		except:
			print("Setting Undefined")
			#pass
		else:
			self.house.setTempParams(ui_config.temp_warn,ui_config.temp_crit)
		try:
			ui_config.list_len
		except:
			print("Setting Undefined")
		else:
			self.geo.set_list_len(ui_config.list_len)	
		#Position size (does not rescale window DONT USE)
		#try:
		#	ui_config.pos_plot_x, ui_config.pos_plot_y
		#except:
	#		print("Setting Undefined")
			#pass
	#	else:
	#		self.geo.set_plot_size(ui_config.pos_plot_x, ui_config.pos_plot_y)
		try:
			ui_config.good_color, ui_config.warn_color, ui_config.crit_color
		except:
			print("Setting Undefined")
			#pass
		else:
			self.house.setColors(ui_config.good_color, ui_config.warn_color, ui_config.crit_color) 
		
		try:
			ui_config.x_color, ui_config.y_color, ui_config.z_color,ui_config.pos_color,ui_config.neg_color
		except:
			print("Setting Undefined")
		else:
			self.controls.setColors(ui_config.x_color, ui_config.y_color, ui_config.z_color,ui_config.pos_color,ui_config.neg_color)
			
		try:
			ui_config.previewx, ui_config.previewy
		except:
			print("Setting Undefined")
		else:
			self.vision.setPreviewSize(ui_config.previewx, ui_config.previewy)
			
		try:
			ui_config.lat0, ui_config.lon0, ui_config.h0
		except:
			print("Setting Undefined")
		else:
			self.geo.set_latlon0(ui_config.lat0, ui_config.lon0, ui_config.h0)
							
	def makeGrid(self):
		self.house.setMaximumHeight(250)
		"""
		#Remove RAVEN Logo for now
		raven = QLabel(self)
		raven.setPixmap(QPixmap(path.abspath(path.dirname(__file__) + "/.." + '/icons/Raven_large.png')).scaled(650, 200, Qt.KeepAspectRatio))
		raven.setAlignment(Qt.AlignCenter)
		raven.setMaximumHeight(70)
		"""
		self.main_widget = QWidget(self)
		maingrid = QGridLayout()
		maingrid.setSpacing(5)
		#maingrid.addWidget(raven,0,0,Qt.AlignTop)
		
		hgrid = QHBoxLayout()
		vgrid1 = QVBoxLayout()
		vgrid2 = QVBoxLayout()
		
		vgrid1.addWidget(self.geo)
		vgrid1.addWidget(self.house)
		vgrid2.addWidget(self.controls)
		vgrid2.addWidget(self.vision)
		
		hgrid.addLayout(vgrid2)
		hgrid.addLayout(vgrid1)
		maingrid.addLayout(hgrid,1,0)
		self.main_widget.setLayout(maingrid)
	
	def aboutEvent(self,event):
		AboutStr = 'This application was developed by the 2017-2018 RAVEN Aerospace Senior Projects Team at the University of Colorado, Boulder. It is a user interface for the control and monitoring of a UAV and UGV pair.'
		about = QMessageBox.information(self, 'About this application', AboutStr)
		self.raven_img = QPixmap(path.abspath(path.dirname(__file__) + "/.." + 'icons/Raven_Large.png'))

	def initActions(self):
		#Exit action
		self.exitAct = QAction(QIcon(path.abspath(path.dirname(__file__) + "/.." + '/icons/exit.png')), 'Exit', self)
		self.exitAct.setShortcut('Ctrl+Q')
		self.exitAct.setStatusTip('Exit application')
		self.exitAct.triggered.connect(self.close_event)
		self.statusBar()
		
		#About action
		self.aboutAct = QAction(QIcon(path.abspath(path.dirname(__file__) + "/.." + '/icons/gtk-info.svg')), 'About',self)
		self.aboutAct.setShortcut('Ctrl+A')
		self.aboutAct.setStatusTip('About this application')
		self.aboutAct.triggered.connect(self.aboutEvent)
		
		#Rreload config action
		self.configAct = QAction(QIcon(path.abspath(path.dirname(__file__) + "/.." + '/icons/gtk-refresh.png')), 'Refresh Settings',self)
		self.configAct.setShortcut('Ctrl+R')
		self.configAct.setStatusTip('Reload ui_config.py and apply settings')
		self.configAct.triggered.connect(self.update_settings)
		
		#Activate Settings Editor
		self.configEditAct = QAction(QIcon(path.abspath(path.dirname(__file__) + "/.." + '/icons/gnome-display-properties.svg')), 'Edit Config',self)
		self.configEditAct.setShortcut('Ctrl+E')
		self.configEditAct.setStatusTip('Open dialogue to edit ui_config.py')
		self.configEditAct.triggered.connect(self.initConfigEditor)
	
	def initConfigEditor(self):
		self.config_edit.exec_()
		
	def initMenu(self):
		#Make the menu
		self.menubar = self.menuBar()
		
		#File Menu
		self.fileMenu = self.menubar.addMenu('&File')
		self.fileMenu.addAction(self.exitAct)
		self.fileMenu.addAction(self.aboutAct)
		self.fileMenu.addAction(self.configAct)
		self.fileMenu.addAction(self.configEditAct)
	
	def initToolbar(self):
		# Make the toolbar
		self.toolbar = self.addToolBar('App Toolbar')
		self.toolbar.addAction(self.exitAct)
		self.toolbar.addAction(self.aboutAct)
		self.toolbar.addAction(self.configAct)	
		self.toolbar.addAction(self.configEditAct)
		
	def initUI(self):
		#self.setStyleSheet("background-color: grey;")
		self.setCentralWidget(self.main_widget)
		self.setGeometry(0,0,1250,875) #default size
		self.setWindowTitle('RAVEN UAV User Interface')	#Title	
		self.setWindowIcon(QIcon(path.abspath(path.dirname(__file__) + "/.." + '/icons/RAVEN_WHITE.png')))
		self.show()	

if __name__ == '__main__':
	#initialize a QApplication
	app = QApplication(sys.argv)
	chatter = uav_ui()
	#Exit the program when app exits
	sys.exit(app.exec_())

