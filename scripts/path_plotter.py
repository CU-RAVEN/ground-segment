#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI
#Implements Position_Plot w/ PyQt and ROS

import sys
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import*# Figure
import matplotlib.pyplot as plt
from matplotlib import gridspec


class Chat_disp(QWidget):
	
	#Initialize classwide data structures
	uav_x = []
	uav_y = []
	uav_z = []	
	ugv_x = []
	ugv_y = []
	ugv_z = []
	
	#signal to update uav & ugv positions
	uav_add_pos = pyqtSignal(float,float,float)
	ugv_add_pos = pyqtSignal(float,float,float)

	#Define the QWidget that will display "talker" message
	def __init__(self):					#upon initialization....
		super(QWidget,self).__init__() 	#Initialize object
		
		self.initFig()	#Initialize matlibplot
		self.initROS_listener_node()	#Initialize ROS subscriber node
		self.initUI() #Function to make the widget
		self.initSig()
		
	def initSig(self):
		#connect signals to their slots (callback functions)
		self.uav_add_pos.connect(self.setUavPos)
		self.ugv_add_pos.connect(self.setUgvPos)

	def setUavPos(self,x,y,z):
		#when uav update signal called
		self.uav_x.append(x)
		self.uav_y.append(y)
		self.uav_z.append(z)
		self.plot_uav()
		
	def setUgvPos(self,x,y,z):
		#when ugv update signal called
		self.ugv_x.append(x)
		self.ugv_y.append(y)
		self.ugv_z.append(z)
		self.plot_ugv()	
								
	def initROS_listener_node(self):
		#Initialize ROS node 'listener':
		rospy.init_node('listener', anonymous=True)	
		#Subscribe node to topic 'random'
		#call callback(self,data) when "random" is updated
		rospy.Subscriber('position_uav', Vector3, self.callback_uav)
		rospy.Subscriber('position_ugv', Vector3, self.callback_ugv)
		
	def initFig(self):
		#declare the plot
		self.fig = plt.figure()
		self.canvas = FigureCanvas(self.fig)#make a figure canvas
		self.canvas.setParent(self)	#not necessary...?		
		self.gs = gridspec.GridSpec(1, 2, width_ratios=[5, 1])  #make the subplot distribution
		self.ax1 = plt.subplot(self.gs[0])	#init subplots w/5:1 distribution
		self.ax2 = plt.subplot(self.gs[1])
		
		#ax1 info (position plot)
		self.ax1.axhline(y=0,color = 'k',linewidth = 2) #cheap way to make x axis
		self.ax1.axvline(x=0,color = 'k',linewidth = 2) #cheap way to make y axis
		self.ax1.set_title('UAV and UGV Location')	#title
		self.ax1.axis('equal')	#same ecale axes
		self.ax1.set_ylabel('Y position [m]')
		self.ax1.set_xlabel('X position [m]')
		#Dummy values to plot before reveiving anything (so the remove() function doesn't break.....)
		self.uav_path = self.ax1.plot(self.uav_x,self.uav_y,':',label = "UAV Path",linewidth = 2.5,color='red')	
		self.ugv_path = self.ax1.plot(self.ugv_x,self.ugv_y,':',label = "UGV Path",linewidth = 2.5,color='green')	
		self.uav_loc = self.ax1.plot(self.uav_x,self.uav_y,'*',color = 'red',markersize = 25)
		self.ugv_loc = self.ax1.plot(self.ugv_x,self.ugv_y,'*',color="green",markersize = 25)
		self.ax1.legend(loc="upper right")	#place legend
		self.ax1.axes.set_xlim([-3,3])		#axis limits
		self.ax1.axes.set_ylim([-3,3])		#axis limits
		
		
		#ax2 info (altitude plot)
		self.bar_width = 0.6				#bar graph widths
		self.ax2.set_xticks([1,2])			#we have two bars, so 2 xticks
		self.ax2.set_xticklabels(['UAV', 'UGV'])	#x labels
		self.ax2.set_ylabel('Altitude [m]')
		self.ax2.set_title('Altitude')
		self.ax2.axes.set_ylim([0,5])			#axis limits
		self.ax2.yaxis.set_label_position("right")	#move stuff to right to avoid clash with position plot
		self.ax2.yaxis.tick_right()
		#dummy values to init plot....
		self.alt_bar = self.ax2.bar([1,2],[0,0],self.bar_width, color = ['red','green'])

		self.canvas.draw()

	def plot_uav(self):
		#Remove plotted lines, stars
		self.ax1.lines.remove(self.uav_loc[-1])
		self.ax1.lines.remove(self.uav_path[-1])
		
		#plot new data
		self.uav_path = self.ax1.plot(self.uav_x,self.uav_y,':',label = "UAV Path",linewidth = 2.5,color='red')
		self.uav_loc = self.ax1.plot(self.uav_x[-1],self.uav_y[-1],'*',color = 'red',markersize = 25)
		self.alt_bar[0].set_height(self.uav_z[-1])
		self.canvas.draw()		
		
	def plot_ugv(self):
		#remove plotted lines, stars
		self.ax1.lines.remove(self.ugv_loc[-1])	
		self.ax1.lines.remove(self.ugv_path[-1])
	
		#plot new data
		self.ugv_path = self.ax1.plot(self.ugv_x,self.ugv_y,':',label = "UGV Path",linewidth = 2.5,color='green')
		self.ugv_loc = self.ax1.plot(self.ugv_x[-1],self.ugv_y[-1],'*',color="green",markersize = 25)
		self.alt_bar[1].set_height(self.ugv_z[-1])
		self.canvas.draw()
			
	def initUI(self):	#Starts UI	
		grid = QGridLayout()	#Top grid layout for widgets
		grid.setSpacing(15)
		
		#Make a "Quit' button to exit application
		quitButton = QPushButton('Quit',self)
		#Exit the program upon clicking 'quitButton'
		quitButton.clicked.connect(QCoreApplication.instance().quit)
		quitButton.resize(quitButton.sizeHint())
		
		#ORganize Widgets in layout
		grid.addWidget(self.canvas,0,0)	#add the figure canvas
		grid.addWidget(quitButton,1,0)
		
		#Add main window layout, set properties
		self.setLayout(grid)
		self.setGeometry(600,600,700,600) #default size
		self.setWindowTitle('Listener')	#Title
		
	def callback_uav(self,data):
		#called when 'uav' is updated
		self.uav_add_pos.emit(data.x,data.y,data.z)	
		
	def callback_ugv(self,data):
<<<<<<< HEAD
		self.ugv_add_pos.emit(data.x,data.y,data.z)		

=======

		self.ugv_add_pos.emit(data.x,data.y,data.z)		


>>>>>>> edcc549c0c78d9763c358822c3ef759a044d2a45

		

if __name__ == '__main__':
	#initialize a QApplication
	app = QApplication(sys.argv)
	chatter = Chat_disp()
	chatter.show() 		#self.show() #Display the widget--now in main as opposed to initUI
	#Exit the program when app exits
	sys.exit(app.exec_())




