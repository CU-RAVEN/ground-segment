#!/usr/bin/env python
#UAV UI load-in
import rospy
from std_msgs.msg import*
from geometry_msgs.msg import*
from sensor_msgs.msg import*
from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from rosbag import*


def initROS_listener_node(self):
	#Initialize ROS node 'listener':
	rospy.init_node('uav_ui', anonymous=True)	
	
	#Subscribe node to topic 'random'
	#call callback(self,data) when "random" is updated
	rospy.Subscriber('position_uav', Vector3, self.geo.callback_uav)
	rospy.Subscriber('position_ugv', Vector3, self.geo.callback_ugv)
	
	#Housekeeping
	rospy.Subscriber('uav_temp',Float64, self.house.emitTemp)
	rospy.Subscriber('uav_storage',Vector3, self.house.emitStore)
	rospy.Subscriber('uav_battery',Vector3, self.house.emitBat)
	rospy.Subscriber('uav_flight_time',Vector3,self.house.emitTime)		
	
	#Commands
	rospy.Subscriber('uav_vel_input',Vector3, self.controls.callback_uav_vel)	
	rospy.Subscriber('uav_command_mode',Bool,self.controls.emitControlMode)
	rospy.Subscriber('uav_power_mode',Bool,self.controls.emitPowerMode)
	rospy.Subscriber('uav_emergency_mode',Bool,self.controls.emitEmergencyMode)
	rospy.Subscriber('uav_rf_strength',Vector3,self.controls.emitNetworkQuality)
	rospy.Subscriber('uav_gps_strength',Vector3,self.controls.emitGPSQuality)
	
	#Imaging
	rospy.Subscriber('uav_tracking_status',Bool,self.vision.emitTrackingStatus)
	rospy.Subscriber('uav_pos_est',Vector3,self.vision.emitPosEst)
	rospy.Subscriber('uav_tracking_timer',Float64,self.vision.emitTrackingTimer)
	rospy.Subscriber('uav_preview_image',Image,self.vision.emitPreviewImage)	
	#rospy.Subscriber('/uav/camera/image_raw',Image,self.vision.emitPreviewImage)
	#rospy.Subscriber('/uav/camera/image_raw/compresesed',CompressedImage,self.vision.emitPreviewImageCompressed)	
