#!/usr/bin/env python

#Author: Alexander Swindell
#Email: alexander.swindell@colorado.edu
#Update: 11.14.2017
#Makes a ROS subscriber node that listens to 'chatter' and displays
# in a PyQt UI Module 1 class (Position)
#Implements Position_Plot w/ PyQt and ROS

import sys
from os import path
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.figure import*# Figure
#import matplotlib.pyplot as plt
#from matplotlib import gridspec

class Config_Box(QDialog):
	
	def __init__(self, parent=None):
		super(QDialog, self).__init__()
		self.myconfig = open(path.abspath(path.dirname(__file__) + '/ui_config.py'),'r')
		

		self.buttonBox = QDialogButtonBox(self)
		self.buttonBox.setOrientation(Qt.Horizontal)
		self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Save)
		
		self.cancel_btn = self.buttonBox.button(QDialogButtonBox.Cancel)
		self.cancel_btn.clicked.connect(self.close)
		self.save_btn = self.buttonBox.button(QDialogButtonBox.Save)
		self.save_btn.clicked.connect(self.save_func)
		
		
		
		self.textBox = QTextEdit(self)
		self.textBox.setText("This is a QTextBrowser!")
		
		with self.myconfig:
			text = self.myconfig.read()
			self.textBox.setText(text)
		self.myconfig.close()
		self.verticalLayout = QVBoxLayout(self)
		self.verticalLayout.addWidget(self.textBox)
		self.verticalLayout.addWidget(self.buttonBox)
		self.setGeometry(100,100,600,750)	
	def save_func(self):
		self.config = open(path.abspath(path.dirname(__file__) + '/ui_config.py'),'w')
		with self.config:
			text = self.textBox.toPlainText()
			self.config.write(text)
		self.config.close()
		self.close()	
	
	def close_func(self):
		self.close()		

